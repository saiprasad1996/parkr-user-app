/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import Navigator from './js/services/router'

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import authReducer from './js/store/reducer';

const store = createStore(authReducer)

class App extends React.Component{

  render(){
    return(
      <Provider store={store}>
        <Navigator />
      </Provider>
    )
  }


}

export default App;