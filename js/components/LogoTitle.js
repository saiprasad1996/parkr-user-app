

import React from 'react'
import {
  Image,
  View,
  Dimensions,
  TouchableOpacity
} from 'react-native'
import {withNavigation} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';

class LogoTitle extends React.Component {


  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        {this.props.withDrawer?<TouchableOpacity onPress={()=>{
          this.props.navigation.toggleDrawer();
        }}><Icon style={{fontSize:20,marginLeft:20,marginTop:6,marginRight:10}} name="bars" /></TouchableOpacity>
        :null}
        <Image
          source={require('../assets/images/logo.png')}
          style={{ resizeMode: 'contain', width: 65, height: 35 }}
        />
        {/* <TouchableOpacity><Icon style={{fontSize:20,marginTop:5,marginLeft:Dimensions.get('window').width-160}} name="bell" /></TouchableOpacity> */}
        {/* <TouchableOpacity onPress={()=>{
          this.props.navigation.navigate('Subscriptions')
        }}><Icon style={{fontSize:20,marginTop:5,marginLeft:35}} name="ellipsis-v" /></TouchableOpacity> */}
      </View>
    );
  }
}


export default withNavigation(LogoTitle)