import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native'


export default class MarkerPopup extends React.Component {

    render() {
        return (
            <View style={styles.container} >
                
                <Text style={{color:"#000000",fontSize:15,fontWeight:'bold'}}>{this.props.location_name}</Text>
                <TouchableOpacity style={{color:"#ffc73e"}}>
                   <Text>Book Now</Text>
                </TouchableOpacity>

            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor:"#fff",
        // backgroundColor: "#ffc73e"
    }
})