import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Overlay } from 'react-native-elements'


export default function PopupCard(props) {
    return (

        <View style={{ flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center", ...props.style }} >
            <Text style={{ fontSize: 20 }}>{props.message}</Text>
            <View style={{flexDirection:"row"}}>
                <TouchableOpacity onPress={props.onPress} style={{borderRadius:20,backgroundColor:"#000",borderWidth:1,padding:5,paddingRight:10,paddingLeft:10}}>
                    <Text style={{color:"#fff"}}>
                        Close
                </Text>
                </TouchableOpacity>
            </View>
        </View>

    )
}