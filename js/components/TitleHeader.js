import React from 'react';
import {View, Dimensions, TouchableOpacity, Text} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between'}}>
        <Text style={{fontSize:20,alignSelf:'stretch',width:150,textAlign:'left',fontWeight:'bold'}}>
          {this.props.title}
        </Text>
        {/* <TouchableOpacity>
          <Icon
            style={{
              fontSize: 20,
              marginTop: 5,
              marginRight:25
            }}
            name="bell"
          />
        </TouchableOpacity> */}
        {/* <TouchableOpacity>
          <Icon
            style={{fontSize: 20, marginTop: 5, marginLeft: 35}}
            name="ellipsis-v"
          />
        </TouchableOpacity> */}
      </View>
    );
  }
}
