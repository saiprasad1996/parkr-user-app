import React from 'react';
import {View, Image, StyleSheet, Text, Dimensions,TouchableOpacity} from 'react-native';
import Car from '../assets/images/car.png';
import Bike from '../assets/images/bike.png';
import {Button,Icon} from 'react-native-elements'
export default function VehicleCard(props) {
  return (
    <View style={{...styles.container, ...props.cardStyle}}>
      {/* <View style={{borderTopLeftRadius:18,borderTopRightRadius:18,backgroundColor:"#fff",borderWidth:3,borderColor:"#000",flexDirection: 'row'}}> */}
      {
        props.vehicleType===2?
        <Image
        source={Car}
        style={styles.image}
      />:
      <Image
      source={Bike}
      style={styles.image}/>
      }
      <View
      style={{marginLeft:15,}}
      >
        <Text
          style={{
            marginTop: 4,
            fontFamily:'Lato-Black',
            textAlign: 'left',
            marginLeft: 10,
            marginTop: 10,
            alignSelf:'stretch',
            alignItems:'center',
            fontSize: 25,
          }}>
          {props.registration_number}
        </Text>

        <Text
          style={{
            textAlign: 'left',
            paddingLeft: 15,
            marginTop: 5,
            fontFamily: 'Lato-Bold',
            fontSize: 18,
            alignSelf:'stretch',
            alignItems:'center',
            color: '#555',
          }}>
          {props.model}
        </Text>
       
      </View>
      <View style={{flex:1,flexDirection:'row',padding:20,justifyContent:'flex-end'}}>
        <TouchableOpacity style={{}}
            onPress={
              props.deleteVehicle
          }
            >
              <Icon name='delete' style={{marginTop:15}}/>
              
             
            </TouchableOpacity>
            </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    flexDirection: 'row',
    borderWidth: 1,
    borderTopWidth: 0,
    paddingLeft: 10,
    paddingBottom: 22,
    backgroundColor: '#fff',
    elevation: 8,
  },
  image:{width: 60, height: 60,justifyContent:'center',alignItems:'center',marginTop:10, resizeMode: 'contain'}
});
