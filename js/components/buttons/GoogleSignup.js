import React from 'react';

import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';

export default class GoogleSignup extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={styles.raised} onPress={this.props.onPress}>
        <Text>Google</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  raised: {
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: {height: 1, width: 1}, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: '#fff',
    elevation: 2, // Android
    height: 50,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
