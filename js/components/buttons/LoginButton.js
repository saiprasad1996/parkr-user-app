import React from 'react'

import { StyleSheet,Text,TouchableOpacity } from 'react-native'

export default class LoginButton extends React.Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
            <TouchableOpacity>
                <Text>
                    Login
                </Text>
            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    buttonColor:{
        backgroundColor:'#ffc73e'
    }
})