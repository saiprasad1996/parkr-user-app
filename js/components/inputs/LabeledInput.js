import React from 'react';
import {View, TextInput, Text, Dimensions} from 'react-native';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';

class LabeledInput extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          width: Dimensions.get('window').width - 50,
          borderStyle: 'solid',
          borderWidth: 1,
          borderRadius: 7,
          marginTop: 10,
          padding:3,
          flexWrap: 'wrap',
          ...this.props.style,
        }}>
        <Text
          style={{
            marginLeft: 25,
            marginTop: -13,
            opacity: 1,
            alignSelf:'baseline',
            backgroundColor: '#fff',
            padding: 5,
            paddingTop: 0,
            paddingRight: 2,
            paddingLeft:2,
            color: 'black',
            fontFamily:'Lato-Regular',
            flexWrap: 'wrap',
            paddingBottom: 5,
          }}>
          {this.props.label}
        </Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Icon
            style={{margin: 10, marginTop: 2}}
            name={this.props.iconName}
            size={20}
            color="#9a9a9a"
          />
          <TextInput
            {...this.props}
            style={{marginTop: -12,flex:1, backgroundColor: 'transparent', zIndex: 2}}
            placeholder={this.props.placeholder}
          />
        </View>
      </View>
    );
  }
}

class PasswordLabeledInput extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      secure:true
    }
  }
  render() {
    return (
      <View
        style={{
          width: Dimensions.get('window').width - 50,
          borderStyle: 'solid',
          borderWidth: 1,
          borderRadius: 7,
          marginTop: 10,
          padding:3,
          flexWrap: 'wrap',
          ...this.props.style,
        }}>
        <Text
          style={{
            marginLeft: 25,
            marginTop: -13,
            opacity: 1,
            alignSelf:'baseline',
            backgroundColor: '#fff',
            padding: 5,
            paddingTop: 0,
            paddingLeft:2,
            color: 'black',
            fontFamily:'Lato-Regular',
            paddingRight: 2,
            flexWrap: 'wrap',
            paddingBottom: 5,
          }}>
          {this.props.label}
        </Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Icon
            style={{margin: 10, marginTop: 2}}
            name={this.props.iconName}
            size={20}
            color="#9a9a9a"
          />
          <TextInput
            {...this.props}
            style={{marginTop: -12,flex:1, backgroundColor: 'transparent', zIndex: 2}}
            placeholder={this.props.placeholder}
            secureTextEntry={this.state.secure}
          />
          <TouchableOpacity onPress={()=>{
            const secure = this.state.secure;
            this.setState({secure:!secure})
          }}>
          <Icon
            style={{margin: 10, marginTop: 2}}
            name={"eye"}
            size={18}
            color="#1a1919"
          />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class LabledFormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      secure:true
    }
  }
  render() {
    return (
      <View
        style={{
          width: Dimensions.get('window').width - 50,
          borderStyle: 'solid',
          borderWidth: 1,
          borderRadius: 7,
          marginTop: 10,
          padding:3,
          flexWrap: 'wrap',
          ...this.props.style,
        }}>
        <Text
          style={{
            marginLeft: 25,
            marginTop: -13,
            opacity: 1,
            alignSelf:'baseline',
            backgroundColor: '#fff',
            padding: 5,
            paddingTop: 0,
            paddingLeft:2,
            color: 'black',
            fontFamily:'Lato-Regular',
            paddingRight: 2,
            flexWrap: 'wrap',
            paddingBottom: 5,
          }}>
          {this.props.label}
        </Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Icon
            style={{margin: 10, marginTop: 2}}
            name={this.props.iconName}
            size={20}
            color="#9a9a9a"
          />
          
          {this.props.children}
          <TouchableOpacity onPress={()=>{
            const secure = this.state.secure;
            this.setState({secure:!secure})
          }}>
          <Icon
            style={{margin: 10, marginTop: 2}}
            name={"eye"}
            size={18}
            color="#1a1919"
          />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}



export default LabeledInput;
export {PasswordLabeledInput,LabledFormInput};