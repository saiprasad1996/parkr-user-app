import React from 'react';

import {
  TextInput,
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class SearchBox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{...styles.container, ...this.props.style}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Icon
            style={{
              marginTop: 20,
              marginLeft: 20,
              marginRight: 10,
              color: '#6B6B6B6B',
              fontSize: 15,
            }}
            name="search"
          />
          <TextInput
            style={{fontSize: 18,width:Dimensions.get('window').width-160 ,fontWeight: 'bold',fontFamily:'Lato-Bold'}}
            onFocus={this.props.onFocus}
            onChangeText={this.props.onChangeText}
            placeholder="Enter for search"
          />
          <TouchableOpacity onPress={this.props.onSearchPress}>
            <Text
              style={{
                borderRadius: 20,
                backgroundColor: '#000',
                marginTop:10,
                padding: 10,
                color: '#ffffff',
                fontFamily:'Lato-Bold',
                overflow:'hidden'
              }}>
              Search
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    elevation: 5,

    backgroundColor: '#fff',
    height: 60,
    color: 'white',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#ddd',
    width: Dimensions.get('window').width - 40,
  },
  search: {
    borderRadius: 20,
    backgroundColor: '#000',
    width: 50,
    padding: 5,
  },
});
