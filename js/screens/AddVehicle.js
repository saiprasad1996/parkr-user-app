import React from 'react';
import {View, StyleSheet,Text, Image, Dimensions, Picker,ActivityIndicator,BackHandler} from 'react-native';
import CarImage from '../assets/images/car.png';
import BikeImage from '../assets/images/bike.png';
import LabeledInput,{LabledFormInput} from '../components/inputs/LabeledInput';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import {ValidateRC} from '../services/constants';
import {Button,Icon} from 'react-native-elements';
import VehicleCard from '../components/VehicleCard';
import {userAddVehicles} from '../services/api';
import AsyncStorage from '@react-native-community/async-storage';

export default class AddVehicle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vehicle_types: [
        {name: '🚗 Car', id: 2},
        {name: '🛵 Bike', id: 1},
      ],
      models: [
        {name: 'Hatchback', id: 'HATCHBACK'},
        {name: 'Sedan', id: 'SEDAN'},
        {name: 'SUV', id: 'SUV'},
      ],
      // colors: ['Black', 'Red', 'Blue', 'Grey', 'Yellow', 'Green', 'Violet'],
      vehicle_selected: 2,
      model_selected: 'HATCHBACK',
      color_selected: '',
      access_token: '',
      registration_number: '',
      loading:false,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    
    try {
      AsyncStorage.getItem('access_token').then(access_token => {
        if (access_token === null) {
          this.props.navigation.navigate('Login');
        } else {
          this.setState({access_token: access_token});
        }
      });
    } catch (error) {
      alert('There is some problem while authorization : ASYNC');
    }
  }

  componentWillUnmount(){
    clearInterval(this.interval);
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    // this.props.navigation.pop();
    this.props.navigation.goBack()
    return true;
  };

  parseErrorString=(successResponse)=>{
    let errorMessage = "";
            const keys= Object.keys(successResponse.messages)
            keys.map((key)=>{
              successResponse.messages[key].map(value=>{
                errorMessage += value+"\n"
              })
            })
            return errorMessage
  }


  addNewVehicle = () => {
    this.setState({loading:true})
    if (this.state.registration_number === '') {
      alert('Please enter the registration number to continue');
      this.setState({loading:false})
     } 
    //else if(!ValidateRC(this.state.registration_number)){
    //   alert('Please Enter a Valid Registered Number' + '\n' + 'Eg.- MH 05 GE 1234');
    //   this.setState({loading:false})
    // }
    else {
      // alert(JSON.stringify(this.state))
      console.log(JSON.stringify(this.state));
      userAddVehicles(
        this.state.access_token,
        this.state.registration_number,
        this.state.vehicle_selected,
        this.state.model_selected,
        success => {
          this.setState({loading:false})
          if(success.status==="success"){
            alert("Vehicle Added successfully")
          }else{
            alert(this.parseErrorString(success));
          
          }
          
        },
        error => {
          this.setState({loading:false})
          alert(error);
        },
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.vehicle_selected == 2?
          <Image source={CarImage} style={{marginTop: 40}} />
          :
          <Image source={BikeImage} style={{marginTop: 40}} />
        }
        <View
          style={{
            borderWidth: 2,
            borderRadius: 8,
            marginTop: 40,
            borderColor: '#000',
            borderWidth:1,

          }}>
            <Text numberOfLines={1} style={{marginTop:-10,backgroundColor:'#fff',alignSelf:'baseline',marginLeft:25,fontFamily:"Lato-Regular",paddingLeft:5,paddingRight:5}}>Select Vehicle</Text>

          <Picker
            selectedValue={this.state.vehicle_selected}
            style={{height: 50,marginBottom:5, width: Dimensions.get('window').width - 50}}
            onValueChange={(itemValue, itemIndex) =>
              // console.log(itemValue,itemIndex)
              this.setState({vehicle_selected: itemValue})
            }>
            {this.state.vehicle_types.map((value, index) => {
              return (
                <Picker.Item
                  label={value.name}
                  key={value.id}
                  value={value.id}
                />
              );
            })}
          </Picker>

        </View>
        {this.state.vehicle_selected == 2?
        <View style={{marginTop: 20,borderColor:'#000',borderWidth:1,borderRadius: 8,}}>
          <Text numberOfLines={1} style={{marginTop:-10,backgroundColor:'#fff',alignSelf:'baseline',marginLeft:25,fontFamily:"Lato-Regular",paddingLeft:5,paddingRight:5}}>Select Model</Text>
          <Picker
            selectedValue={this.state.model_selected}
            style={{height: 50,marginBottom:5, width: Dimensions.get('window').width - 50}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({model_selected: itemValue})
            }>
            {this.state.models.map((value, index) => {
              return (
                <Picker.Item
                  label={value.name}
                  key={value.id}
                  value={value.id}
                />
              );
            })}
          </Picker>
        </View>:null}
        <View style={{flexDirection:'row',marginTop:20}}>
          <LabeledInput
            style={{height: 60}}
            label="Registration Number"
            placeholder="MH 05 GE 1234"
            onChangeText={text => {
              this.setState({registration_number: text});
            }}
          />
        </View>
        {/* 
        <Picker
          selectedValue={this.state.color_selected}
          style={{height: 50, width: Dimensions.get('window').width - 40}}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({language: itemValue})
          }>
          {this.state.colors.map((value, index) => {
            return <Picker.Item label={value} key={value} value={value} />;
          })}
        </Picker> */}
        {this.state.loading ? <ActivityIndicator style={{marginTop:10,marginBottom:10}} size="small" color="#000000" />:<React.Fragment/> }
        <Button
          title="Add"
          raised
          onPress={this.addNewVehicle}
          titleStyle={{color: 'black', fontWeight: 'bold'}}
          buttonStyle={{
            borderColor: '#ffc73e',
            backgroundColor: '#ffc73e',
            borderRadius: 6,
          }}
          containerStyle={{
            marginTop: 40,
            width: Dimensions.get('window').width - 50,
          }}
        />

        {/* <VehicleCard
            registration_number="MH 02AW 1235"
            name="Hyundai i20"
            model="Hatchback"

          /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
});
