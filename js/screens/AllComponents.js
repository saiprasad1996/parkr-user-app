import React from 'react'
import {
    View, Button,Text

} from 'react-native'
import FacebookSignup from '../components/buttons/FacebookSignup'

export default class AllComponents extends React.Component{

    constructor(props){
        super(props)
    }
    render(){
        return(
            <View>
                <FacebookSignup onPress={
                    (event)=>{
                        this.props.navigation.navigate("Drawer")
                    }
                } />
                <Button title={"Login"} onPress={()=>this.props.navigation.navigate('Login')}/>
            </View>

        )
    }
}