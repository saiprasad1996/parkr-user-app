import React, {Component} from 'react';
import {View,Text,Image, Dimensions,TouchableOpacity,ToastAndroid,ActivityIndicator,BackHandler, StyleSheet} from 'react-native';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import GoogleMapImage from '../assets/images/googlemap.jpg';
import IndiaCarLogo from '../assets/images/vehicle_logo.png';
import {Divider} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ScrollView} from 'react-native-gesture-handler';
import {
  locationDetailsById,
  userVehicles,
  bookingRequest,
} from '../services/api';
import DateTimePicker from '@react-native-community/datetimepicker';
import getDirections from 'react-native-google-maps-directions';
import {withNavigationFocus} from 'react-navigation';
import Arrowright from '../assets/images/arrowright.png'
import Arrowleft from '../assets/images/arrowleft.png';
import { NavigationEvents } from 'react-navigation';
import {connect} from 'react-redux'

import { CheckBox } from 'react-native-elements'


class Book extends Component {
  constructor(props) {
    super(props);
    this.state = {
      access_token:this.props.access_token,
      from_time: '',
      to_time: '',
      value: this.props.navigation.state.params.value,
      date: new Date(),
      mode: 'date',
      show: false,
      time_mode: '',
      vehicles: [],
      today: this.getDate(),
      index_date: 0,
      vehicle_type: '',
      selected_vehicle: '',
      vehicle_model: '',
      start_date: this.getDate(),
      end_date: '',
      today_str: '',
      timer: '',
      loading: false,
      closes_in:'',
      today_check:true,
      tomorrow_check:false
    
    };
  }

  componentDidUpdate(prevProps) {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
   
    if (prevProps.isFocused !== this.props.isFocused) {
      this.getUserVehicles()
    }
  }

  

  getUserVehicles(){
    userVehicles(
      this.props.access_token,
      response => {
        if (response.status == 'error') {
          console.log(response.message);
        } else {
          console.log('User vehicles', response);
          const press_statuses=[];
          for (let i=0;i<response.vehicles.length;i++){
            press_statuses.push(false)
          }
          this.setState({vehicles: response.vehicles});
        }
      },
      error => {
        console.log(error);
      },
    );
  }

  onBackPress = () => {
    // this.props.navigation.pop();
    this.setState({value:''})
    this.props.navigation.navigate('Home');
    return true;
  };

  parseErrorString=(successResponse)=>{
    let errorMessage = "";
            const keys= Object.keys(successResponse.messages)
            keys.map((key)=>{
              successResponse.messages[key].map(value=>{
                errorMessage += value+"\n"
              })
            })
            return errorMessage
  }

  init_date = () => {
    const today = new Date();
    today.setDate(today.getDate() - 0);
    let today_str = today.toISOString();
    today_str = today_str.split('T')[0];
    this.setState({today: today.toDateString(), today_str: today_str});
  };

  from_time = () => {
    let time = new Date();
    time = time.setTime(time.getTime() + 1 * 60 * 60 * 1000);
    time = moment(time).format('HH:mm');
    this.setState({from_time: time.toString()});
  };
  to_time = () => {
    let time = new Date();
    time = time.setTime(time.getTime() + 1.5 * 60 * 60 * 1000);
    time = moment(time).format('HH:mm');
    this.setState({to_time: time.toString()});
  };

  getTime = () => {
    const date = new Date();
    return `${this.appendzero(date.getHours())}:${this.appendzero(
      date.getMinutes(),
    )}`;
  };

  getTimeStr = date => {
    return `${this.appendzero(date.getHours())}:${this.appendzero(
      date.getMinutes(),
    )}`;
  };

  getDate = () => {
    const date = new Date();
    return date.toDateString();
  };


  am_pmString=(date_str)=>{
    return moment(date_str, ["HH:mm"]).format("hh:mm A");
    
  }


  componentDidMount() {
    this.from_time();
    this.to_time();
    this.init_date();
    console.log('this.state.value')
    console.log(this.state.value)

    ToastAndroid.show('Please wait...', ToastAndroid.LONG);
    id = this.state.value.id;
    try {
      AsyncStorage.getItem('access_token').then(access_token => {
        if (access_token === null) {
          this.props.navigation.navigate('Login');
        } else {
          this.setState({access_token: access_token});
          userVehicles(
            access_token,
            response => {
              if (response.status == 'error') {
                console.log(response.message);
              } else {
                console.log('User vehicles', response);
                this.setState({vehicles: response.vehicles});
              }
            },
            error => {
              console.log(error);
            },
          );
        }
      });
    } catch (error) {}

    locationDetailsById(
      id,
      sResponse => {
        console.log(sResponse);
        const location_data = {...sResponse.location};
        this.setState({...location_data});
        this.setState({closes_in:this.state.closing_time})
      },
      error => {
        alert("Unable to fetch location details. Please try again");
      },
    );
  }

  bookSlot = () => {
    if( this.state.available_slots === 0){
      alert('Sorry no available slots for booking')
      return
    }
    this.setState({loading:true})
    let start_date = this.state.today_str;
    let end_date = this.state.today_str;
    if (this.state.selected_vehicle == '') {
      alert('Please select vehicles!!');
    } else {
      bookingRequest(
       
      this.props.access_token,
        start_date,
        end_date,
        this.state.from_time,
        this.state.to_time,
        this.state.value.id,
        this.state.selected_vehicle,
        this.state.vehicle_type,
        success => {
          this.setState({loading:false})
          console.log(success);
          if (success.status === 'success') {
            // alert(
            //   `Your booking request has been saved! Please proceed for payment. Your request id is ${success.request_id}`,
            // );
            this.props.navigation.navigate('Payment', {
              request_id: success.request_id,
              value: {...this.state.value,address_line1:this.state.address_line1,from_time:this.state.from_time,to_time:this.state.to_time,date:this.state.today_str,
                from_time_12hr:this.am_pmString(this.state.from_time),to_time_12hr:this.am_pmString(this.state.to_time)},
            });
          } else if(success.status==="error") {
            if (success.messages !== undefined){
            alert(this.parseErrorString(success));
            // alert(JSON.stringify(success));
          }else if (success.message!==undefined){
            alert(success.message)
          }
          }
        },
        error => {
          this.setState({loading:false})
          console.log(error);
          alert(JSON.stringify(error));
        },
      );
    }
  };

  setDate = (event, date) => {
    date = date || this.state.date;
    if (this.state.time_mode === 'from') {
      this.setState({
        show: Platform.OS === 'ios' ? true : false,
        from_time: this.getTimeStr(date),
      });
    } else if ((this.state.time_mode = 'to')) {
      this.setState({
        show: Platform.OS === 'ios' ? true : false,
        to_time: this.getTimeStr(date),
      });
    }
  };

  appendzero = n => {
    if (n <= 9) return '0' + n;
    else return n;
  };

  show = (mode, time_mode) => {
    this.setState({
      show: true,
      mode,
      time_mode: time_mode,
    });
  };

  timepicker = time_mode => {
    this.show('time', time_mode);
  };

  componentWillUnmount(){
    clearInterval(this.interval);
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  render() {
    const {show, date, mode} = this.state;
    return (
      <ScrollView style={{backgroundColor: '#fffffd'}}>
       <NavigationEvents
                onDidFocus={() => {
                  this.getUserVehicles()
                }}
                />
        <View
          style={{
            marginTop: 20,
            borderWidth: 1,
            borderTopLeftRadius: 50,
            borderTopRightRadius: 50,
            borderColor: '#ddd',
            backgroundColor:"#fff",
            borderBottomColor: '#fff',
          }}>
          {show && (
            <DateTimePicker
              value={date}
              mode={mode}
              is24Hour={false}
              display="default"
              onChange={this.setDate}
            />
          )}
          <Divider
            style={{
              backgroundColor: '#ddd',
              width: 100,
              marginTop: 20,
              alignSelf: 'center',
              height: 5,
              borderRadius: 20,
            }}
          />
          <TouchableOpacity
          onPress={() => {
            const data = {
              destination: {
                latitude: parseFloat(this.state.latitude),
                longitude: parseFloat(this.state.longitude),
              },
            };
            getDirections(data);
          }}>
          <View
            style={{
              width: screenWidth - 30,
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 30,
              borderColor: '#ddd',
              borderWidth: 1,
              borderRadius: 50,
              height:70,
              paddingRight: 15,
              borderBottomWidth:4,
              borderRightWidth:3,
            }}>
            <Image
              source={GoogleMapImage}
              style={{
                width: 70,
                height: 67,
                borderTopLeftRadius: 50,
                borderBottomLeftRadius: 50,
                
              }}
            />
            <View style={{flexDirection: 'column', padding: 10}}>
              <Text numberOfLines={1} style={{fontFamily: 'Lato-Bold', width: screenWidth * 0.5,fontSize:14}}>
                {this.state.name}
              </Text>
              <Text
                numberOfLines={2}
                style={{
                  width: screenWidth * 0.5,
                  fontFamily: 'Lato-Regular',
                  textAlign: 'justify',fontSize:12
                }}>
                {this.state.address_line1}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                const data = {
                  destination: {
                    latitude: parseFloat(this.state.latitude),
                    longitude: parseFloat(this.state.longitude),
                  },
                };
                getDirections(data);
              }}>
              <Icon
                name="location-arrow"
                type="FontAwesome"
                style={{fontSize: 30, marginTop: 18}}
              />
            </TouchableOpacity>
          </View>
          </TouchableOpacity>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Text style={{fontSize: 16, fontFamily: 'Lato-Bold', color: '#fece41'}} >
              Capacity - {this.state.capacity}
            </Text>
            <Text
              style={{fontSize: 16, fontFamily: 'Lato-Bold', color: '#fece41'}}>
              Available - {this.state.available_slots}
            </Text>
          </View>

          {/* <View style={{paddingTop:15,paddingLeft:10,paddingRight:10,flex:1,flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color: '#000',fontFamily:"Lato-Bold", fontSize: 13}}>Opening time - {this.state.value.opening_time}</Text>
        <Text style={{color: '#000',fontFamily:"Lato-Bold", fontSize: 13}}>Closing time - {this.state.value.closing_time} </Text>
          
      </View> */}
          <Divider
            style={{
              borderColor: '#ddd',
              width: screenWidth,
              marginTop: 20,
              alignSelf: 'center',
              borderWidth: 2,
              borderStyle: 'dashed',
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Text style={{fontFamily: 'Lato-Black', marginBottom: 20}}>
              PARKING TIME
            </Text>
            {/* <Text style={{fontFamily: 'Lato-Regular'}}>
              Closes in
              <Text style={{color: '#fece41', fontFamily: 'Lato-Regular'}}>
                {' '}
                {this.state.closes_in}
              </Text>
            </Text> */}
          </View>

          <View
            style={{
              width: screenWidth - 30,
              alignSelf: 'center',
              flexDirection:'row',
              justifyContent:'space-between'
            }}>
            <TouchableOpacity
              onPress={() => {
                this.timepicker('from');
              }}
              style={{
                borderRadius: 20,
                borderColor: '#e5e5e5',
                borderTopWidth:1,
                borderLeftWidth:2,
                borderRightWidth:3,
                borderBottomWidth:3,
                paddingLeft: 15,
                paddingRight:15,
                paddingTop:8,
                paddingBottom:8,
                
              }}>
              <Text
                style={{color: '#000',paddingTop:25, fontSize: 11, fontFamily: 'Lato-Bold'}}>
                From
              </Text>
              <Text style={{fontFamily: 'Lato-Bold',fontSize:14 ,paddingTop: 5}}>
                {this.am_pmString(this.state.from_time)}
              </Text>
              <Text>{''}</Text>
            </TouchableOpacity>
            
            <TouchableOpacity
              onPress={() => {
                this.timepicker('to');
              }}
              style={{
                borderRadius: 20,
                borderColor: '#e5e5e5',
                borderTopWidth:1,
                borderLeftWidth:2,
                borderRightWidth:3,
                borderBottomWidth:3,
                paddingLeft: 15,
                paddingRight:15,
                paddingTop:8,
                paddingBottom:8,
              }}>
                
              <Text
                style={{color: '#000',paddingTop:25, fontSize: 11, fontFamily: 'Lato-Bold'}}>
                To
              </Text>
              <Text style={{fontFamily: 'Lato-Bold',fontSize:14, paddingTop: 5}}>
                {this.am_pmString(this.state.to_time)}
              </Text>
              <Text>{''}</Text>
             
            </TouchableOpacity>
            
            <View
              style={{
                borderRadius: 20,
                borderColor: '#ddd',
                borderWidth: 2,
                padding: 15,
                paddingTop:8,
                borderStyle: 'dashed',
              }}>
              <Text
                style={{color: '#000', paddingTop:10, fontSize: 11, fontFamily: 'Lato-Bold'}}>
                Opens at
              </Text>
              <Text
                style={{color: '#000', paddingTop: 5, fontFamily: 'Lato-Bold'}}>
                {this.state.opening_time}
              </Text>


              <Text
                style={{color: '#000', fontSize: 11,marginTop:5, fontFamily: 'Lato-Bold'}}>
                Closes at
              </Text>
              <Text
                style={{color: 'red', paddingTop: 5, fontFamily: 'Lato-Bold'}}>
                {this.state.closing_time}
              </Text>
              <Text>{''}</Text>
            </View>
          </View>

          <View style={{marginTop: 20}}>
            <Text
              style={{
                fontFamily: 'Lato-Black',
                paddingLeft: 15,
                marginBottom: 20,
              }}>
              PARKING DATE
            </Text>
              

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignSelf: 'center',
                width: screenWidth - 30,
                borderColor: '#ddd',
                borderWidth: 1,
                borderRadius: 20,
                borderBottomWidth:4,
              borderRightWidth:3,
              }}>
              <TouchableOpacity
                onPress={() => {
                  const tomorrow = new Date();
                  tomorrow.setDate(tomorrow.getDate() - 0);
                  let today_str = tomorrow.toISOString();
                  today_str = today_str.split('T')[0];
                  this.setState({
                    today: tomorrow.toDateString(),
                    today_str: today_str,
                  });
                }}
               
                >
                  
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  flexDirection: 'column',
                  width: screenWidth / 2,
                  padding: 10,
                }}>
                {this.state.today == this.getDate() ? (
                  <Text style={{fontFamily: 'Lato-Black'}}>Today</Text>
                ) : (
                  <Text style={{fontFamily: 'Lato-Black'}}>Tomorrow</Text>
                )}

                <Text style={{fontFamily: 'Lato-Regular'}}>
                  {this.state.today}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  const tomorrow = new Date();
                  tomorrow.setDate(tomorrow.getDate() + 1);
                  let today_str = tomorrow.toISOString();
                  today_str = today_str.split('T')[0];
                  this.setState({
                    today: tomorrow.toDateString(),
                    today_str: today_str,
                  });
                }}
                >
                   
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignSelf: 'center',
                width: screenWidth - 30,
               }}  >
                  
                  <CheckBox
                    center
                    title='Today'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.today_check}
                    onPress={() => {
                      const tomorrow = new Date();
                      tomorrow.setDate(tomorrow.getDate() - 0);
                      let today_str = tomorrow.toISOString();
                      today_str = today_str.split('T')[0];
                      this.setState({
                        today: tomorrow.toDateString(),
                        today_str: today_str,
                        today_check:true,
                        tomorrow_check:false
                      });
                    }}

                  />
                  <CheckBox
                    center
                    title='Tomorrow'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.tomorrow_check}
                    onPress={() => {
                      const tomorrow = new Date();
                      tomorrow.setDate(tomorrow.getDate() + 1);
                      let today_str = tomorrow.toISOString();
                      today_str = today_str.split('T')[0];
                      this.setState({
                        today: tomorrow.toDateString(),
                        today_str: today_str,
                        today_check:false,
                        tomorrow_check:true
                      });
                    }}

                  />

              </View>

          </View>

          <View style={{marginTop: 20, marginLeft: 15}}>
            <Text style={{fontFamily: 'Lato-Black', marginBottom: 20}}>
              SELECT YOUR VEHICLE
            </Text>
            <React.Fragment>
              <ScrollView horizontal={true}>
                {this.state.vehicles.map((value, index) => (
                    <TouchableOpacity key={index}
                      onPress={() => {
                        this.setState({
                          vehicle_model: value.model,
                          selected_vehicle: value.id,
                          vehicle_type: value.vehicle_type_id,
                          pressStatus:true,
                        });
                      }}
                      style={
                        this.state.selected_vehicle == value.id
                            ? styles.buttonPress
                            : styles.button
                      }>
                      {/* <Image
                        source={IndiaCarLogo}
                        style={{
                          width: 40,
                          height: 52,
                          borderTopLeftRadius: 18,
                          borderBottomLeftRadius: 18,
                        }}
                      /> */}
                      <Text
                        style={{
                          fontFamily: 'Lato-Black',
                          fontSize: 20,
                          padding: 15,
                        }}>
                        {value.registration_number === ""
                          ? 'N/A'
                          : value.registration_number}
                      </Text>
                    </TouchableOpacity> 
                ))}
                {/* {this.state.selected_vehicle == value.id ? (
                      <Icon
                        type="FontAwesome"
                        name="check"
                        style={{
                          color: '#ffc73e',
                          fontSize: 20,
                          position: 'absolute',
                          alignSelf: 'flex-start',
                        }}></Icon>
                    ) : null} */}
                <View style={{alignSelf: 'center'}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('AddVehicle')}
              style={{
                flexDirection: 'row',
                borderColor: '#aaa',
                borderRadius: 20,
                borderWidth: 2,
                borderStyle: 'dashed',
                padding: 20,
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <Icon
                style={{color: '#aaa', textAlign: 'center'}}
                type="FontAwesome"
                name="plus"></Icon>
              <Text style={{color: '#aaa', fontFamily: 'Lato-Bold'}}>
                {' '}
                Add your Vehicle
              </Text>
            </TouchableOpacity>
          </View>
              </ScrollView>
              {/* <Text style={{marginTop: 5}}>
                <Icon
                  name="check"
                  type="FontAwesome"
                  style={{color: 'gold'}}></Icon>
                {this.state.vehicle_model}
              </Text> */}
            </React.Fragment>
          </View>
{/* 
          <View style={{alignSelf: 'center', marginTop: 20}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('AddVehicle')}
              style={{
                flexDirection: 'row',
                borderColor: '#aaa',
                borderRadius: 20,
                borderWidth: 2,
                borderStyle: 'dashed',
                padding: 20,
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <Icon
                style={{color: '#aaa', textAlign: 'center'}}
                type="FontAwesome"
                name="plus"></Icon>
              <Text style={{color: '#aaa', fontFamily: 'Lato-Bold'}}>
                {' '}
                Add your Vehicle
              </Text>
            </TouchableOpacity>
          </View> */}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
              padding: 15,
            }}>
            <Text
              style={{
                width: screenWidth / 2,
                color: '#aaa',
                fontFamily: 'Lato-Italic',
              }}>
              {""}
            </Text>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: 100,
                backgroundColor: 'gold',
                borderRadius: 10,
                padding: 15,
                paddingLeft:18
              }}
             
              onPress={this.bookSlot}>
              {false ? (
                <ActivityIndicator
                  style={{marginTop: 10, marginBottom: 10}}
                  size="small"
                  color="#000"
                />
              ) : (
                <React.Fragment>
                  <Text style={{fontFamily: 'Lato-Black',fontSize:20}}>Book</Text>
                  <Image
                    style={{color:"black"}}
                      source={Arrowright}
                      style={{marginLeft:5,resizeMode:'contain', width: 15, height: 15 }}
                    />
                  {/* <Icon
                    type="FontAwesome"
                    name="angle-right"
                    style={{fontSize: 25, marginLeft: 10}}></Icon> */}
                    
                </React.Fragment>
                
              )}
            </TouchableOpacity>
            
          </View>
        </View>
       
      </ScrollView>
    );
  }
}

const BookNav = withNavigationFocus(Book);

const styles = StyleSheet.create({
  button:{
    backgroundColor: '#fff',
    flexDirection: 'row',
    borderRadius: 20,
    borderWidth: 2,
    borderColor:'#f1f1f1',
    marginRight: 15,
  },
  buttonPress:{
    backgroundColor: 'gold',
    flexDirection: 'row',
    borderRadius: 20,
    marginRight: 15,
  }
});

const mapPropsToState = store => {
  return {
    access_token: store.access_token,
    phone: store.phone,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    
  };
};

export default connect(mapPropsToState, mapDispatchToProps)(BookNav);