import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {ACCESS_TOKEN} from '../services/constants';
import {allBookings, currentBooking} from '../services/api';

import {StackActions, NavigationActions} from 'react-navigation';
import {Overlay, Image} from 'react-native-elements';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class Bookings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current_bookings: [],
      past_bookings: [],
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({loading: true});
    AsyncStorage.getItem(ACCESS_TOKEN).then(token => {
      if (token === null) {
        this.props.navigation.navigate('Login');
      } else {
        allBookings(
          token,
          success => {
            if (success.status === 'success') {
              console.log(success);
              this.setState({
                past_bookings: success.data.past_bookings,
                current_bookings: success.data.current_bookings,
              });
            } else {
              ToastAndroid.show('Error occured while fetching data');
            }
            this.setState({loading: false});
          },
          error => {
            this.setState({loading: false});
            alert(error);
            console.log(error);
          },
        );

        currentBooking(
          token,
          success => {
            if (success.status === 'success') {
              console.log('Current bookings', success);

              // this.setState({current_bookings:success.data.})
            }
          },
          error => {
            console.log(error);
            alert(error);
          },
        );
      }
    });
  }

  getFromAndToTimeStr = (start_date_time, end_date_time) => {
    const s = start_date_time.split(' ');
    const e = end_date_time.split(' ');

    return s[1] + ' to ' + e[1];
  };
  render() {
    return (
      <ScrollView style={{backgroundColor: '#f1f1f1'}}>
        {/* Add code for showing qr code. The qr code and booking details will be available in the state */}
        {/* <Overlay
          isVisible={this.state.isVisible}
          windowBackgroundColor="rgba(255, 255, 255, .5)"
          overlayBackgroundColor="red"
          width="auto"
          height="auto"
        >
          <Image
            source={{  }}
            style={{ width: 200, height: 200 }}
            PlaceholderContent={<ActivityIndicator />}
          />
          <Text>Hello from Overlay!</Text>
        </Overlay> */}

        <View
          style={{
            width: screenWidth - 30,
            alignSelf: 'center',
            paddingTop: 40,
          }}>
          <Text style={{marginBottom: 30, fontWeight: 'bold'}}>ACTIVE</Text>

          {this.state.loading ? (
            <React.Fragment>
              <ActivityIndicator
                style={{marginTop: 10, marginBottom: 10}}
                size="small"
                color="#000000"
              />
              <Text style={{textAlign: 'center'}}>
                Please wait while we are loading...
              </Text>
            </React.Fragment>
          ) : (
            <React.Fragment />
          )}

          {this.state.current_bookings.length === 0 &&
          this.state.loading === false ? (
            <View style={{alignItems: 'center', marginTop: 30}}>
              <Text>No current bookings found!</Text>
            </View>
          ) : (
            this.state.current_bookings.map((value, index) => {
              console.log('booking details');
              console.log(value);
              return (
                <TouchableOpacity
                  key={index}
                  style={{
                    borderColor: '#ddd',
                    borderWidth: 1,
                    flexDirection: 'row',
                    backgroundColor: '#fff',
                    borderRadius: 15,
                    marginTop: 20,
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('Ticket', {
                      value: value,
                      parking_at: value.location.name,
                      from_time: value.start_date_time.split(' '),
                      to_time: value.end_date_time.split(' '),
                      amount: value.amount,
                      qrcode: value.qrcode,
                      from_bookings: true,
                      request_id: value.id,
                      location_id:value.location_id
                    });
                  }}>
                  <View
                    style={{
                      width: screenWidth / 3,
                      borderRightColor: '#ddd',
                      borderRightWidth: 1,
                      borderStyle: 'dashed',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 25,
                        marginTop: 20,
                        alignSelf: 'stretch',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}>
                      {'\u20B9'} {value.amount}
                    </Text>
                  </View>
                  <View style={{padding: 15}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      On{' '}
                      <Text style={{color: '#aaa'}}>
                        {' ' + value.start_date_time}
                      </Text>
                    </Text>
                    <Text
                      numberOfLines={2}
                      style={{
                        fontWeight: 'bold',
                        color: '#aaa',
                        fontSize: 12,
                        width: screenWidth / 2,
                      }}>
                      Parking at{' '}
                      <Text numberOfLines={2} style={{color: '#000'}}>
                        {' ' + value.location.name}
                      </Text>
                    </Text>
                    <Text
                      style={{fontWeight: 'bold', color: '#aaa', fontSize: 12}}>
                      From{' '}
                      <Text style={{color: '#000'}}>
                        {this.getFromAndToTimeStr(
                          value.start_date_time,
                          value.end_date_time,
                        )}
                      </Text>
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })
          )}

          {/* <TouchableOpacity
            style={{
              borderColor: '#ddd',
              borderWidth: 1,
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderRadius: 15,
            }}>
            <View
              style={{
                width: screenWidth / 3,
                borderRightColor: '#ddd',
                borderRightWidth: 1,
                borderStyle: 'dashed',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 35, marginTop: 16, fontWeight: 'bold'}}>
                {'\u20B9'} 250
              </Text>
            </View>
            <View style={{padding: 15}}>
              <Text style={{fontWeight: 'bold', fontSize: 12}}>
                On <Text style={{color: '#aaa'}}>1 Nov 2019</Text>
              </Text>
              <Text style={{fontWeight: 'bold', color: '#aaa', fontSize: 12}}>
                Parking at <Text style={{color: '#000'}}>Lodha Supremus</Text>
              </Text>
              <Text style={{fontWeight: 'bold', color: '#aaa', fontSize: 12}}>
                From <Text style={{color: '#000'}}>10:00 am to 01:00 pm</Text>
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              borderColor: '#ddd',
              borderWidth: 1,
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderRadius: 15,
              marginTop: 20,
            }}>
            <View
              style={{
                width: screenWidth / 3,
                borderRightColor: '#ddd',
                borderRightWidth: 1,
                borderStyle: 'dashed',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 35, marginTop: 16, fontWeight: 'bold'}}>
                {'\u20B9'} 250
              </Text>
            </View>
            <View style={{padding: 15}}>
              <Text style={{fontWeight: 'bold', fontSize: 12}}>
                On <Text style={{color: '#aaa'}}>1 Nov 2019</Text>
              </Text>
              <Text style={{fontWeight: 'bold', color: '#aaa', fontSize: 12}}>
                Parking at <Text style={{color: '#000'}}>Lodha Supremus</Text>
              </Text>
              <Text style={{fontWeight: 'bold', color: '#aaa', fontSize: 12}}>
                From <Text style={{color: '#000'}}>10:00 am to 01:00 pm</Text>
              </Text>
            </View>
          </TouchableOpacity> */}
        </View>

        <View
          style={{
            width: screenWidth - 30,
            alignSelf: 'center',
            paddingTop: 40,
          }}>
          <Text style={{marginBottom: 30, fontWeight: 'bold', color: '#aaa'}}>
            ENDED
          </Text>
          {this.state.loading ? (
            <React.Fragment>
              <ActivityIndicator
                style={{marginTop: 10, marginBottom: 10}}
                size="small"
                color="#000000"
              />
              <Text style={{textAlign: 'center'}}>
                Please wait while we are loading...
              </Text>
            </React.Fragment>
          ) : (
            <React.Fragment />
          )}

          {this.state.past_bookings.length === 0 &&
          this.state.loading === false ? 
            
            <View style={{alignItems: 'center', marginTop: 30}}>
              <Text>No past booking found!</Text>
            </View>
          : (
            this.state.past_bookings.map((value, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={{
                    borderColor: '#ddd',
                    borderWidth: 1,
                    marginBottom: 20,
                    flexDirection: 'row',
                    backgroundColor: '#fff',
                    borderRadius: 15,
                    borderStyle: 'dashed',
                  }}>
                  <View
                    style={{
                      width: screenWidth / 3,
                      borderRightColor: '#ddd',
                      borderRightWidth: 1,
                      borderStyle: 'dashed',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 25,
                        marginTop: 20,
                        fontWeight: 'bold',
                        color: '#aaa',
                      }}>
                      {'\u20B9'} {value.amount}
                    </Text>
                  </View>
                  <View style={{padding: 15}}>
                    <Text
                      style={{fontWeight: 'bold', color: '#aaa', fontSize: 12}}>
                      On
                      <Text style={{color: '#ddd'}}>
                        {' ' + value.start_date_time}
                      </Text>
                    </Text>
                    <Text
                      numberOfLines={2}
                      style={{
                        fontWeight: 'bold',
                        color: '#ddd',
                        fontSize: 12,
                        width: screenWidth / 2,
                      }}>
                      Parking at
                      <Text numberOfLines={2} style={{color: '#aaa'}}>
                        {' '}
                        {' ' + value.location.name}
                      </Text>
                    </Text>
                    <Text
                      style={{fontWeight: 'bold', color: '#ddd', fontSize: 12}}>
                      From
                      <Text style={{color: '#aaa'}}>
                        {' '}
                        {this.getFromAndToTimeStr(
                          value.start_date_time,
                          value.end_date_time,
                        )}
                      </Text>
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })

          )}
        </View>
      </ScrollView>
    );
  }
}
