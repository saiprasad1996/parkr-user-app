import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
  Alert,
  Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../assets/images/logo.png';
import LabeledInput from '../components/inputs/LabeledInput';
import {getOtp} from '../services/api';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      loading: false,
    };
  }

  showAlert=()=>{
    Alert.alert(
        'Password Recovery',
        'We have sent you an OTP to your registered phone number. Please enter the OTP received in the next screen',
        [
         
          {text: 'OK', onPress: () => this.props.navigation.navigate('Otp',{phone:this.state.phone})},
        ],
        {cancelable: false},
      );
  }

  openExternalURL(url) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          ToastAndroid.show("Can't handle url: " + url, ToastAndroid.LONG);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  getResetLink = () => {
    this.setState({loading: true});
    if (this.state.phone === '') {
      ToastAndroid.show('Please fill in the phone number', ToastAndroid.LONG);
      this.setState({loading: false});
    } else {
      getOtp(
        this.state.phone,
        success => {
            if(success.status==="success"){
                this.showAlert()
                this.setState({loading: false,});
            }else{
                this.setState({loading: false});
                alert("Phone number invalid/ not available. Please try again")
            }
            // this.showAlert()
        //   alert(JSON.stringify(success));
          
        },
        error => {
          ToastAndroid.show(error, ToastAndroid.LONG);
          this.setState({loading: false});
        },
      );
    }
  };

  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <View style={styles.container}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <View
          style={{
            borderColor: '#000',
            borderWidth: 1,
            borderRadius: 20,
            alignSelf: 'center',
            width: screenWidth - 30,
            padding: 15,
            marginTop: 50,
          }}>
          <View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between',marginTop:5}}>
              <Text style={{fontWeight: 'bold',marginBottom:15}}>Password Recovery</Text>
              {/* <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                style={{
                  backgroundColor: '#fd5e53',
                  flexDirection: 'row',
                  width:40,
                  height:40,
                  padding: 12,
                  paddingLeft:14,
                  borderRadius: 20,
                }}>
                  
                <Icon style={{color: '#fff', fontSize: 15}} name="close"></Icon>
              </TouchableOpacity> */}
            </View>

            <LabeledInput
              placeholder="Enter Your Registered Phone Number"
              label="Phone Number"
              style={{
                width:Dimensions.get('window').width-65
              }}
              iconName="user-circle-o"
              keyboardType="phone-pad"
              maxLength={13}
              onChangeText={phone => {
                this.setState({phone: phone});
              }}
            />
            <Text style={{marginTop: 30}}>
              Enter your phone number to get your OTP to recover your account
            </Text>
            {this.state.loading ? (
              <ActivityIndicator
                style={{marginTop: 10, marginBottom: 10}}
                size="small"
                color="#000000"
              />
            ) : (
              <React.Fragment />
            )}

            <TouchableOpacity
              onPress={this.getResetLink}
              style={{
                borderRadius: 15,
                backgroundColor: 'gold',
                padding: 15,
                alignItems: 'center',
                marginTop: 20,
              }}>
              <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                Send Reset Link
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={()=>{
                this.props.navigation.goBack()
              }}
              style={{
                
                padding: 15,
                alignItems: 'center',
                marginTop: 10,
              }}>
              <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
              <Icon style={{color: '#000', fontSize: 15}} name="arrow-left"></Icon>   Go back
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 40,
              marginBottom: 10,
            }}>
            <Text style={{width: screenWidth / 2}}>
              If you are facing any issue please contact Support
            </Text>
            <TouchableOpacity
              // onPress={()=>this.props.navigation.navigate("Ticket")}
              // onPress={()=>this.props.navigation.navigate("Profile")}
              // onPress={()=>this.props.navigation.navigate("PasswordUpdate")}
              // onPress={() => this.props.navigation.navigate('PaymentSuccess')}
              // onPress={()=>this.props.navigation.navigate("PaymentError")}
              onPress={()=>{
                this.openExternalURL(
                  'http://booking.parkr.in/help'
                );
              }}
              style={{backgroundColor: '#000', padding: 10, borderRadius: 10}}>
              <Text style={{color: '#fff'}}>Help !</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 200,
    marginTop: 30,
    resizeMode: 'contain',
  },
});
