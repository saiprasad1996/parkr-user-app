import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  KeyboardAvoidingView,
  Text,
  ToastAndroid,
  Keyboard,
  Image,
  PermissionsAndroid,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import MapView, {Marker} from 'react-native-maps';

import SearchBox from '../components/inputs/SearchBox';
import {Overlay, Button} from 'react-native-elements';
import PopupCard from '../components/PopupCard';
import MarkerIcon from '../assets/images/map_marker_black.webp';
import {locationsAll} from '../services/api';
import CurrentPositionMarker from '../assets/images/position.png'
import {locationSearch} from '../services/api'
import {getDistance} from '../services/utils'


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const latitude = 19.118533;
const longitude = 72.869505;
const lat_delta = 0.0008993216059187306;
const long_delta = 0.0009594731963041548;
// const long_delta = lat_delta* (width / height);

const searchBoxMargin = Dimensions.get('window').height - 160;

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'',opening_time:'',closing_time:'',value:[],
      markers: [],
      isVisible: false,
      searchPressOverlay:false,searchData:[],
      searchBoxMargin: searchBoxMargin,
      searchToken: '',
      initialRegion: {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: lat_delta,
        longitudeDelta: long_delta,
      },
      region: {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: lat_delta,
        longitudeDelta: long_delta,
      },
      stretch: {
        
        resizeMode: 'stretch'
      },
      current_position:{
        latitude:0.0,
        longitude:0.0
      }
    };
  }

  componentDidMount() {
    ToastAndroid.show(
      'Please wait while we load all the available locations',
      ToastAndroid.LONG,
    );
    locationsAll(
      sResponse => {
        // console.log("All locations",sResponse);
        this.setState({markers: sResponse.locations});

        this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          this._keyboardDidShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
          'keyboardDidHide',
          this._keyboardDidHide,
        );
        this.userCurrentLocationAndroid();
      },
      error => {},
    );
    this.userCurrentLocationAndroid();

    //   this.setState({region:{
    //     latitude:latitude,
    //     longitude:longitude,
    //     latitudeDelta: lat_delta,
    //     longitudeDelta: long_delta,
    // }})
  }

  async userCurrentLocationAndroid() {
    if (
      await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      )
    ) {
      Geolocation.getCurrentPosition(info => {
        console.log('Permission granted info', info);
        this.setState({
          initialRegion: {
            latitude: info.coords.latitude,
            longitude: info.coords.longitude,
            latitudeDelta: lat_delta,
            longitudeDelta: long_delta,
          },
          region: {
            latitude: info.coords.latitude,
            longitude: info.coords.longitude,
            latitudeDelta: lat_delta,
            longitudeDelta: long_delta,
          },
          current_position:{
            latitude: info.coords.latitude,
            longitude: info.coords.longitude,
          }
        });

        // this.setState({
        //   latitude:info.coords.latitude,
        //   longitude:info.coords.longitude,
        //   error: null,
        // });
      });
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message:
              'Parkr needs access to your location ' +
              'so that we can show a nearest location for parking your vehicle',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          ToastAndroid.show(
            'Parkr will now use your location to better serve you.',
            ToastAndroid.LONG,
          );
          Geolocation.getCurrentPosition(info => {
            console.log(
              console.log('Permission asked and granted info', info),
              info,
            );
            this.setState({
              initialRegion: {
                latitude: info.coords.latitude,
                longitude: info.coords.longitude,
                latitudeDelta: lat_delta,
                longitudeDelta: long_delta,
              },
              region: {
                latitude: info.coords.latitude,
                longitude: info.coords.longitude,
                latitudeDelta: lat_delta,
                longitudeDelta: long_delta,
              },
            });
          });
        } else {
          ToastAndroid.show(
            "Oops! We can't get your location now. :(",
            ToastAndroid.LONG,
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidHide = () => {
    // console.log("Keyboard hide")
    this.setState({searchBoxMargin: searchBoxMargin});
  };

  _keyboardDidShow = () => {
    // console.log("Keyboard show")
    this.setState({searchBoxMargin: 150});
  };

  // searchLocation() {
  //   // console.log(this.state.markers)
  //   if (this.state.searchToken !== '') {
  //     // console.log(this.state.searchToken);
  //     // console.log(JSON.stringify(this.state.markers));
  //     let values = this.state.markers.filter(value =>
  //       value.name.startsWith(this.state.searchToken),
  //     );
  //     // console.log('home page values');
  //     // console.log(values);
  //     this.setState({...values})
  //     if (values.length === 0) {
  //       ToastAndroid.show(
  //         'No location found with the entered value',
  //         ToastAndroid.LONG,
  //       );
  //     } else {
  //       values = values[0];
  //       const region = {
  //         latitude: values.latitude,
  //         longitude: values.longitude,
  //         latitudeDelta: lat_delta,
  //         longitudeDelta: long_delta,
  //       };
  //       this.setState({region: region,searchPressOverlay:true,name:values.name,opening_time:values.opening_time,closing_time:values.closing_time,value:values});

  //       // Alert.alert(
  //       //   'Location Found',
  //       //   `\nName - ${values.name}\nOpening Time - ${values.opening_time}\nClosing Time - ${values.closing_time}`,
  //       //   [
  //       //     {
  //       //       text: 'Book',
  //       //       onPress: () =>
  //       //         this.props.navigation.navigate('Book', {value: values}),
  //       //     },
  //       //     {
  //       //       text: 'Cancel',
  //       //       onPress: () => console.log('Cancel Pressed'),
  //       //       style: 'cancel',
  //       //     },
  //       //   ],
  //       // );
  //     }
  //   }
  // }

  searchLocation() {
    // console.log(this.state.markers)
    Keyboard.dismiss()
    ToastAndroid.show('Please wait while we are searching for the location',ToastAndroid.SHORT)
    if (this.state.searchToken !== '') {
      locationSearch(this.state.searchToken,
        (success)=>{
            console.log(success)
            console.log(success.locations[0].latitude,success.locations[0].longitude)
            const region = {
                      latitude: success.locations[0].latitude,
                      longitude: success.locations[0].longitude,
                      latitudeDelta: lat_delta,
                      longitudeDelta: long_delta,
                    };
                    this.setState({region: region});
                    // ToastAndroid.show(success.message,ToastAndroid.SHORT)

        },
        (error)=>{
          console.log(error)
        })
    }else{
      ToastAndroid.show('Please type something in the search box to search.',ToastAndroid.LONG)
    }
  }


  bookParking(){
    this.setState({searchPressOverlay:false});
    this.props.navigation.navigate('Book', {value: this.state.value});
  }

  pressMarker(value){
    console.log(value.address_line1)
    this.setState({searchPressOverlay:true,name:value.name,address:value.address_line1+" "+value.address_line2,opening_time:value.opening_time,closing_time:value.closing_time,value:value})
  }

  render() {
    return (
      <KeyboardAvoidingView enabled style={styles.container}>
        <Overlay
          overlayBackgroundColor="#ffc73e"
          borderRadius={20}
          width={Dimensions.get('window').width - 50}
          height={200}
          isVisible={this.state.isVisible}>
          <PopupCard
            onPress={() => {
              console.log('Close');
              this.setState({isVisible: false});
            }}
            message={'Booked'}
          />
        </Overlay>
        <Overlay
            borderRadius={20}
            isVisible={this.state.searchPressOverlay}
            onBackdropPress={() => this.setState({ searchPressOverlay : false })}
            windowBackgroundColor="rgba(0, 0, 0, .5)"
            overlayBackgroundColor="#fff"
            width={Dimensions.get('window').width*0.8}
            height="auto"
          ><React.Fragment>
            <Text style={{marginBottom:30,textAlign:'center',fontFamily:'Lato-Black',fontSize:20}}>Location details</Text>
            <View style={{flexDirection:'row'}}>
              <Text style={styles.key}>{"Name : "}</Text>
              <Text style={styles.value}>{this.state.name}</Text>
            </View>
            <View style={{flexDirection:'row'}}>
              <Text style={styles.key}>{"Address : "}</Text>
              
              <Text style={{fontWeight:'bold',flexWrap:'wrap',textAlign:'left',width:Dimensions.get('window').width*0.45}}>{this.state.address}</Text>
            </View>
            <View style={{flexDirection:'row'}}>
              <Text style={styles.key}>{"Opening Time : "}</Text>
              <Text style={styles.value}>{this.state.opening_time}</Text>
            </View>
            <View style={{flexDirection:'row'}}>
              <Text style={styles.key}>{"Closing Time : "}</Text>
              <Text style={styles.value}>{this.state.closing_time}</Text>
            </View>
            <View style={{flexDirection:'row'}}>
              <Text style={styles.key}>{"Distance : "}</Text>
              <Text style={styles.value}>{getDistance(this.state.current_position.latitude,this.state.current_position.longitude,this.state.region.latitude,this.state.region.longitude)+" km"}</Text>
            </View>

            <View style={{flexDirection:'row',justifyContent:'flex-end',marginRight:20,marginTop:30}}>
              <Button title="Book" buttonStyle={{marginRight:20,backgroundColor:'gold',paddingTop:5,paddingBottom:10,paddingLeft:20,paddingRight:20,borderRadius:5}}
              onPress={() => {  this.bookParking()}}/>
              <Button title="Cancel" buttonStyle={{backgroundColor:'#000',paddingTop:5,paddingBottom:10,paddingLeft:15,paddingRight:15,borderRadius:5}}
              onPress={() => {this.setState({searchPressOverlay: false});}}/>
            </View>
            </React.Fragment>
        </Overlay>
        <MapView
          minZoomLevel={10}
          maxZoomLevel={19}
          
          provider="google"
          initialRegion={this.state.initialRegion}
          showsMyLocationButton={true}
          region={this.state.region}
          style={{
            position: 'absolute',
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            zIndex: -1,
          }}>
          {this.state.markers.map((value, index) => {
            // console.log(value)
            if (value.status === 'active') {
              return (
                <Marker
                  key={index}
                  coordinate={{
                    longitude: value.longitude,
                    latitude: value.latitude,
                  }}
                  onPress={() => { this.pressMarker(value)}}
                  image={MarkerIcon}>
                   
                      {/* this.props.navigation.navigate('Book', {value: value}); */}
                    
                    {/* <MarkerPopup location_name={'Name : ' + value.name +'\n' + 'Opening Time : ' + value.opening_time +'\n'+'Closing Time : '+ value.closing_time} /> */}
                </Marker>
              );
            }
            if(this.state.current_position.latitude !== 0.0){
              return (
                <Marker
                  key={index}
                  coordinate={{
                    longitude: this.state.current_position.longitude,
                    latitude: this.state.current_position.latitude,
                  }}
                  image={CurrentPositionMarker} />
                   
              )
            }
          })}
        </MapView>
        <View style={{flex:1,position:'absolute',marginTop:searchBoxMargin-70}}>
        <Button
          containerStyle={{marginLeft:Dimensions.get('window').width-80}}
          onPress={() => {
            this.userCurrentLocationAndroid();
          }}
          icon={ <Image
            style={styles.stretch}
            source={require('../assets/images/relocate.png')}
          />}
          type="clear"
        />
        </View>

        <SearchBox
          onChangeText={text => {
            this.setState({searchToken: text});
          }}
          style={{marginTop: this.state.searchBoxMargin}}
          onFocus={event => {
            console.log(event);
            this.setState({searchBoxMargin: 150});
          }}
          onSearchPress={event => {
            this.searchLocation();
          }}
        />
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 200,
    marginTop: 20,
    resizeMode: 'contain',
  },
  key:{
    width:115,
    alignContent:'stretch',
    paddingRight:5,
    alignSelf:'flex-end',
    textAlign:'right',
    flexWrap:'wrap'
  },
  value:{
    fontWeight:'bold',alignSelf:'flex-start',textAlign:'left',
    flexWrap:'wrap',width:Dimensions.get('window').width*0.4
  }
});
/**
   * <MapView
                minZoomLevel={6}
                provider="google"
                initialRegion={{
                    latitude:latitude,
                    longitude:longitude,
                    latitudeDelta:0.0008993216059187306,
                longitudeDelta:0.0009594731963041548
                }
                }
                style={{height:Dimensions.get('window').height}} />
            
   */
