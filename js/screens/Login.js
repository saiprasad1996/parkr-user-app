import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  ToastAndroid,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Button, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../assets/images/logo.png';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import LabeledInput,{PasswordLabeledInput} from '../components/inputs/LabeledInput';
import {connect} from 'react-redux'

import { LoginManager,AccessToken,GraphRequestManager,GraphRequest } from 'react-native-fbsdk'
import { GoogleSignin, statusCodes } from 'react-native-google-signin';

import {authenticate, socialLogin, userExists,googleLogin} from '../services/api'
import { LOGIN } from '../store/constants';
import { FACEBOOK, GOOGLE } from '../services/constants';

class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      userid: "",
      password: '',
      loading:false
    }
    
  }

  componentDidMount(){
    GoogleSignin.configure({
      scopes: [], webClientId: '883055851534-tqbj1qok3vphj9qd4es0up5m74bu2msj.apps.googleusercontent.com', 
      offlineAccess: true, 
      hostedDomain: '', 
      loginHint: '', 
      forceConsentPrompt: true, 
      accountName: '',
      androidClientId:'883055851534-69hna36epromi4esai797odt7a82v0tl.apps.googleusercontent.com',
      iosClientId: '883055851534-rjqegdc3dm09nkot4b5vubid2ctpo0ch.apps.googleusercontent.com'
      });
  }

  authenticate=()=>{
    this.setState({loading:true})
    const userid = this.state.userid.trim()
    const password= this.state.password.trim()
    if (userid==="" || password===""){
      this.setState({loading:false})
      ToastAndroid.show('Please enter both username and password to login', ToastAndroid.LONG);
    }else{
      authenticate(userid,password,(successResponse)=>{
        this.setState({loading:false})
          console.log(successResponse)
          if(successResponse.status === "success"){
          ToastAndroid.show('Authentication success',ToastAndroid.LONG)
          try{
          AsyncStorage.setItem('access_token',successResponse.Authorization)
          AsyncStorage.setItem('phone',this.state.userid)
          this.props.login(successResponse.Authorization,userid)
          this.props.navigation.goBack()
          }catch(error){
            ToastAndroid.show('Error occured while performing internal operation',ToastAndroid.LONG)
          }
        }else{
          ToastAndroid.show('Authentication failed',ToastAndroid.LONG)
        }
      },(errorResponse)=>{
        this.setState({loading:false})
        ToastAndroid.show('Oops Something went wrong',ToastAndroid.LONG)
          console.log(errorResponse)
      })
    }
  }

  handleGoogleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo)
      await googleLogin(userInfo.user.email,
        (success)=>{
          console.log(success)
          if(success.status==="success"){
            ToastAndroid.show('Login successful',ToastAndroid.LONG)
            this.props.login(success.Authorization,String(success.data.mobile_no))
            this.props.navigation.goBack()
            this.setState({loading:false})
          }else{
            alert(success.messages)
            this.setState({loading:false})
          }
            
        },(error)=>{
          alert('There was some error logging you in. Please contact developer '+error)
          this.setState({loading:false})
        })
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('user cancelled')
        alert("Login Cancelled")
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('Login in progress')
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('Play services outdated')
        alert('Google Play services not available')
      } else {
        // some other error happened
        console.log(error)
      }
    }
  };
  
  handleFacebookLogin =()=> {
    this.setState({loading:true})
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
       (result)=> {
        if (result.isCancelled) {
          console.log('Login cancelled')
          this.setState({loading:false})
        } else {
          console.log('Login result: ' , result)
          console.log('Login success with permissions: ' + result.grantedPermissions.toString())
          let accessToken;
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              accessToken = data.accessToken
              console.log(accessToken.toString())
            }
          )

          const responseInfoCallback = (error, result) => {
            if (error) {
              console.log(error)
              alert('Error fetching data: ' + error.toString());
              this.setState({loading:false})
            } else {
              console.log(result)
              // alert('Success fetching data: ' + JSON.stringify(result));
               socialLogin(accessToken,FACEBOOK,
                (success)=>{
                  console.log(success)
                  if(success.status==="success"){
                    ToastAndroid.show('Login successful',ToastAndroid.LONG)
                    try{
                      AsyncStorage.setItem('access_token',success.Authorization)
                      AsyncStorage.setItem('phone',String(success.mobile_no))
                      this.props.login(success.Authorization,success.mobile_no)
                      this.props.navigation.goBack()
                      this.setState({loading:false})
                      }catch(error){
                        console.log(error)
                        ToastAndroid.show('Error occured while performing internal operation',ToastAndroid.LONG)
                      }
                  }else if (success.status==='error'){
                    alert(success.msg)
                    this.setState({loading:false})
                  }
                    
                },(error)=>{
                  alert('There was some error logging you in. Please contact developer '+error)
                  this.setState({loading:false})
                })
        
            }
          }
          
          const infoRequest = new GraphRequest(
            '/me',
            {
              accessToken: accessToken,
              parameters: {
                fields: {
                  string: 'email,name,first_name,middle_name,last_name'
                }
              }
            },
            responseInfoCallback
          );

          // Start the graph request.
          new GraphRequestManager().addRequest(infoRequest).start()
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error)
        this.setState({loading:false})
      }
    )
    
  }


  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#000" barStyle="light-content" />

        <View style={styles.container}>
          <Image source={Logo} style={styles.logo} />
          <Text style={{ color: '#b0b0b0', fontSize: 18,fontFamily:'Lato-Bold' }}>Sign in with</Text>

          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Button
              icon={
                <Icon
                  name="google"
                  style={{ marginRight: 10 }}
                  size={15}
                  color="#ff3a00"
                />
              }
              raised
              type="outline"
              title="Google"
              titleStyle={{ color: 'black',fontFamily:'Lato-Regular' }}
              buttonStyle={{ borderColor: '#e2e2e2' }}
              containerStyle={{ marginTop: 20, marginRight: 20, width: 120 }}
              onPress={()=>{
                this.handleGoogleLogin()
              }}
            />

            <Button
              icon={
                <Icon
                  name="facebook-square"
                  style={{ marginRight: 10 }}
                  size={20}
                  color="#385ea5"
                />
              }
              raised
              type="outline"
              title="Facebook"
              onPress={()=>{
                this.handleFacebookLogin()
              }}
              titleStyle={{ color: 'black',fontFamily:'Lato-Regular' }}
              buttonStyle={{ borderColor: '#e2e2e2' }}
              containerStyle={{ marginTop: 20, width: 120 }}
            />
          </View>
          <View style={styles.lineStyle} />

          <LabeledInput
            placeholder="Type your registered phone number"
            label="User ID"
            iconName="user-circle-o"

            onChangeText={(text) => {
              // console.log(text)
              this.setState({ userid: text })
            }}
          />

          <PasswordLabeledInput
            secureTextEntry={true}
            onChangeText={(text) => {
              // console.log(text)
              this.setState({ password: text })
            }}
            onSubmitEditing={Keyboard.dismiss}
            placeholder="Type your password"
            label="Password"
            style={{ marginTop: 20 }}
            iconName="lock"

          />
        {
          this.state.loading ? <ActivityIndicator style={{marginTop:10,marginBottom:10}} size="small" color="#000000" />:<React.Fragment/> }
            <Button
            title="Login"
            raised
            onPress={this.authenticate}
            titleStyle={{ color: 'black',fontFamily:'Lato-Black' }}
            buttonStyle={{ borderColor: '#ffc73e', backgroundColor: '#ffc73e', borderRadius: 6 }}
            containerStyle={{
              marginTop: 20,
              width: Dimensions.get('window').width - 50,

            }}
          />
          <Text style={{ marginTop: 10,fontFamily:'Lato-Bold' }}>or</Text>
          <Button
            title="Signup"
            onPress={() => this.props.navigation.navigate("Signup")}
            raised
            titleStyle={{ color: '#fff',fontFamily:'Lato-Black'}}
            buttonStyle={{ borderColor: '#ffc73e', backgroundColor: '#000000', borderRadius: 6 }}
            containerStyle={{
              marginTop: 10,
              width: Dimensions.get('window').width - 50,
            }}
          />
          <View style={{flexDirection:"column",alignItems:'center',marginTop:25}}>
          <Text style={{color:"#808080",marginRight:5,fontFamily:'Lato-Regular',fontSize:15}}>Forgot Password?</Text>
          <TouchableOpacity><Text style={{color:"#f2ac13",fontFamily:'Lato-Regular',fontSize:18,marginTop:5}} onPress={()=>this.props.navigation.navigate("ForgotPassword")}>Get New</Text></TouchableOpacity>
          
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginBottom:20
  },
  logo: {
    width: 200,
    marginTop: 30,
    resizeMode: 'contain',
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#dbdbdb',
    margin: 10,
    marginTop: 30,
    marginBottom: 30,
    width: Dimensions.get('window').width - 50,
  },
  loginButton: {
    backgroundColor: '#ffc73e',
  },
});

const mapPropsToState = store => {
  return {
    token: store.access_token,
    phone: store.phone,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: (access_token,phone) => dispatch({type: LOGIN,payload:{access_token:access_token,phone:phone}})
  };
};

export default connect(mapPropsToState, mapDispatchToProps)(Login);