import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Alert,
  ToastAndroid,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Logo from '../assets/images/logo.png';
import {TextInput} from 'react-native-gesture-handler';
import {Button} from 'react-native-elements';
import {forgotPassword} from '../services/api';

export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
      loading: false,
      new_password:"",
      confirm_password:"",
      phone: this.props.navigation.state.params.phone,
    };
  }
  verify_Otp = () => {
    this.setState({loading: true});
    verifyOtp(
      this.state.phone,
      this.state.otp,
      success => {
        if (success.status === 'success') {
          alert(JSON.stringify(success));
          this.setState({loading: false});
        } else {
          this.setState({loading: false});
          alert(JSON.stringify(success));
        }
      },
      error => {
        ToastAndroid.show(error, ToastAndroid.LONG);
        this.setState({loading: false});
      },
    );
  };


  change_password=()=>{
    
    if(this.state.new_password==="" || this.state.confirm_password=== ""){

        alert('Please enter value in both the fields')
        return
    }
    
    this.setState({loading:true});

      forgotPassword(
          this.state.phone,this.state.new_password,this.state.confirm_password,
          success => {
            if (success.status === 'success') {
                Alert.alert(
                    'Password Recovery',
                    'New password reset successful',
                    [
                     
                      {text: 'OK', onPress: () => this.props.navigation.popToTop()},
                    ],
                    {cancelable: false},
                  );
        
            } else {
              this.setState({loading: false});
              console.log(success)
              let message = "";
              success.messages['password'].map((value,index)=>{
                message+=value;
              })
              alert(`${message}`);
              // Alert.alert(
              //   'Password Recovery',
              //   `Un-expected error occurred. Please try again Error Code - ${JSON.stringify(success)}`,
              //   [
                 
              //     {text: 'OK', onPress: () => this.props.navigation.popToTop()},
              //   ],
              //   {cancelable: false},
              // );
            }
          },
          error => {
            ToastAndroid.show(error, ToastAndroid.LONG);
            this.setState({loading: false});
          },
      )


  }


  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems: 'center'}}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <View style={{flexDirection: 'column', alignItems: 'center'}}>
          <Text style={{fontFamily:'Lato-Bold',fontSize:25,paddingBottom:20}}>Set new password</Text>
          <TextInput
            
            placeholder="Mobile Number"
            disabled
            value={this.state.phone}
            style={{
              borderRadius: 15,
              borderWidth: 2,
              borderColor: '#000',
              fontSize: 18,
              paddingLeft: 15,
              fontFamily:'Lato-Regular',
              width: Dimensions.get('window').width - 100,
              marginBottom:20
            }}
          />
          <TextInput
            secureTextEntry={true}
            placeholder="Enter new password"
            onChangeText={password => this.setState({new_password: password})}
            style={{
              borderRadius: 15,
              borderWidth: 2,
              borderColor: '#000',
              fontSize: 18,
              paddingLeft: 15,
              fontFamily:'Lato-Regular',
              width: Dimensions.get('window').width - 100,
              marginBottom:20
            }}
          />
        <TextInput
            placeholder="Confirm new password"
            onChangeText={password => this.setState({confirm_password: password})}
            secureTextEntry={true}
            style={{
              borderRadius: 15,
              borderWidth: 2,
              borderColor: '#000',
              fontSize: 18,
              fontFamily:'Lato-Regular',
              paddingLeft: 15,
              marginBottom:15,
              width: Dimensions.get('window').width - 100,
            }}
          />
    
       
       </View>
        {this.state.loading ? (
          <ActivityIndicator
            style={{marginTop: 10, marginBottom: 10}}
            size="small"
            color="#000000"
          />
        ) : (
          <React.Fragment />
        )}
        <Button
          title="Submit"
          raised
          onPress={this.change_password}
          titleStyle={{color: 'black', fontFamily:'Lato-Bold',fontSize:20, fontWeight: 'bold'}}
          buttonStyle={{
            borderColor: '#ffc73e',
            backgroundColor: '#ffc73e',
            borderRadius: 6,
          }}
          containerStyle={{
            marginTop: 80,
            width: Dimensions.get('window').width - 50,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 200,
    marginTop: 30,
    marginBottom:30,
    resizeMode: 'contain',
  },
});
