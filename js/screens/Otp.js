import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Logo from '../assets/images/logo.png';
import {TextInput} from 'react-native-gesture-handler';
import {Button} from 'react-native-elements';
import {verifyOtp} from '../services/api';

export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      loading: false,
      phone: this.props.navigation.state.params.phone,
    };
  }
  verify_Otp = () => {
    this.setState({loading: true});
    verifyOtp(
      this.state.phone,
      this.state.otp,
      success => {
        if (success.status === 'success') {

            
        //   alert("OTP verified successfully. Please enter a new password in the next screen");
          Alert.alert(
            'Password Recovery',
            'OTP verified successfully. Please enter a new password in the next screen',
            [
             
              {text: 'OK', onPress: () => this.props.navigation.navigate('NewPassword',{phone:this.state.phone})},
            ],
            {cancelable: false},
          );


          this.setState({loading: false});
        } else {
          this.setState({loading: false});
         
          Alert.alert(
            'Password Recovery',
            'Invalid OTP. Please try again',
            [
             
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ],
            {cancelable: false},
          );
        }
      },
      error => {
        ToastAndroid.show(error, ToastAndroid.LONG);
        this.setState({loading: false});
      },
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems: 'center'}}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <View style={{flexDirection: 'column', alignItems: 'center'}}>
          <Text style={{fontFamily:'Lato-Bold',fontSize:25,paddingBottom:20}}>Enter OTP below</Text>
          <TextInput
            maxLength={4}
            keyboardType="numeric"
            onChangeText={otp => this.setState({otp: otp})}
            style={{
              borderRadius: 15,
              borderWidth: 2,
              borderColor: '#000',
              fontSize: 22,
              textAlign: 'center',
              paddingLeft: 15,
              letterSpacing: 42,
              width: Dimensions.get('window').width - 100,
            }}
          />
        
          {/* <TextInput maxLength={1} keyboardType='numeric' onChangeText={(SecondOtp) => this.setState({ SecondOtp })} style={{borderRadius:15,borderWidth:2,borderColor:"#000",fontSize:22,textAlign:'center'}} />

                    <TextInput maxLength={1} keyboardType='numeric' onChangeText={(ThirdOtp) => this.setState({ ThirdOtp })} style={{borderRadius:15,borderWidth:2,borderColor:"#000",fontSize:22,textAlign:'center'}} />

                    <TextInput maxLength={1} keyboardType='numeric' onChangeText={(FourthOtp) => this.setState({ FourthOtp })}  style={{borderRadius:15,borderWidth:2,borderColor:"#000",fontSize:22,textAlign:'center'}} /> */}
        </View>
        {this.state.loading ? (
          <ActivityIndicator
            style={{marginTop: 10, marginBottom: 10}}
            size="small"
            color="#000000"
          />
        ) : (
          <React.Fragment />
        )}
        <Button
          title="Submit"
          raised
          onPress={this.verify_Otp}
          titleStyle={{color: 'black', fontWeight: 'bold'}}
          buttonStyle={{
            borderColor: '#ffc73e',
            backgroundColor: '#ffc73e',
            borderRadius: 6,
          }}
          containerStyle={{
            marginTop: 100,
            width: Dimensions.get('window').width - 50,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 200,
    marginTop: 30,
    resizeMode: 'contain',
  },
});
