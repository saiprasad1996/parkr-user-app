import React from 'react'
import {
    View,
    Text,
    ScrollView,
    StatusBar,Dimensions,TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage'
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import {connect} from 'react-redux'
import { LOGOUT } from '../store/constants';
import { ACCESS_TOKEN } from '../services/constants';


class PasswordUpdate extends React.Component{
    constructor(props){
       super(props)
        this.state={

        }
    }
   
    
    render(){
        return (
            <ScrollView>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                <View style={{alignSelf:'center',marginTop:screenHeight/3,alignItems:'center'}}>
                    <Icon name="check" type="FontAwesome" style={{fontSize:30,color:'green'}}></Icon>
                    <Text style={{marginTop:30}}>Password Updated successfully. Please login again</Text>
                </View>
                <TouchableOpacity onPress={()=>{
                    AsyncStorage.multiRemove([ACCESS_TOKEN, 'phone']).then(
                        returnval => {
                          console.log(returnval);
                          this.props.logout()
                          this.props.navigation.navigate('HomeDrawer');
                          
                          
                        },
                      );
                    this.props.logout();
                    this.props.navigation.popToTop()}}  style={{backgroundColor:'gold',width:screenWidth/2,alignSelf:'center',alignItems:'center',padding:10,borderRadius:10,marginTop:50}}>
                    <Text style={{fontWeight:'bold'}}>
                        Home
                    </Text>
                </TouchableOpacity>
            </ScrollView>
        )
    }
}


const mapPropsToState = store => {
    return {
      token: store.access_token,
      phone: store.phone,
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      logout: () => dispatch({type: LOGOUT})
    };
  };
  
  export default connect(mapPropsToState, mapDispatchToProps)(PasswordUpdate);