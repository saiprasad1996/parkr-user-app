import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Alert,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ToastAndroid,
  ActivityIndicator,
  Platform,
  ImageBackground,
  BackHandler
} from 'react-native';
import {Divider} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';
import LabeledInput from '../components/inputs/LabeledInput';
import Parking from '../assets/images/parking.png';
import GoogleMapImage from '../assets/images/googlemap.jpg';
import TicketCutOut from '../assets/images/ticketcutout.png';
import Arrowright from '../assets/images/arrowright.png'

const screenWidth = Dimensions.get('window').width;
import getDirections from 'react-native-google-maps-directions';
const screenHeight = Dimensions.get('window').height;
import {
  locationDetailsById,
  payment,
  getTransactionDetailByID,
  updateOnlinePayment
} from '../services/api';
import AsyncStorage from '@react-native-community/async-storage';
import {ACCESS_TOKEN} from '../services/constants';
import PaymentSuccess from './PaymentSuccess';
import PaymentError from './PaymentError';

import Paytm from '@philly25/react-native-paytm';

export default class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.navigation.state.params.value,
      booking_id: this.props.navigation.state.params.request_id,
      radioCheck: 0,
      show: false,
      show1: false,
      show2: false,
      show3: false,
      access_token: null,
      loading: false,
      pressStatus: false,
      success:null,
      payment:'',
      access_token:'',
      payment_modes:[]
    };
    console.log('Parameters ', this.props.navigation.state.params.value);
    // this.updateIndex = this.updateIndex.bind(this);
  }


  onPayTmResponse = (resp) => {
    const {STATUS, status, response} = resp;
    this.setState({loading: true})
    if (Platform.OS === 'ios') {
      if (status === 'Success') {
        const jsonResponse = JSON.parse(response);
        const {STATUS} = jsonResponse;

        if (STATUS && STATUS === 'TXN_SUCCESS') {
          // Payment succeed!
          console.log(resp)
        }else{
          console.log(resp)
          alert(resp.RESPMSG)
        }
      }
    } else {
      if (STATUS && STATUS === 'TXN_SUCCESS') {
        // Payment succeed!
        console.log(resp)
        updateOnlinePayment(this.state.access_token,this.state.booking_id,resp,
          (success)=>{
            console.log("Success online update",success)
            if (success.status==="error"){
              alert("Error message",success.messages)
            }else if (success.status==="success"){
              this.setState({loading: false,success:true});
          // Alert.alert('Response', JSON.stringify(success.messages));
          this.props.navigation.navigate('Ticket', {
            value: success.data,
            parking_at: this.state.value.name,
            from_time: this.state.value.from_time,
            to_time: this.state.value.to_time,
            amount:success.data.amount,
            from_time_12hr: this.state.value.from_time_12hr,
            to_time_12hr: this.state.value.to_time_12hr,
            location_id:this.state.value.id
          });
              console.log(success)
            }
          },
          (error)=>{
            console.log(error)
            this.setState({loading:false})
          })
      }else if(status==="Cancel"){
        console.log(resp)
        alert("Transaction cancelled")
        this.setState({loading:false})
      }
    }
  };

  // runTransaction(amount, customerId, orderId, mobile, email, checkSum, mercUnqRef) {
    runTransaction(details) {
    const d = {
        mode: 'Production', // 'Staging' or 'Production'    
        ...details
   };
    
    Paytm.startPayment(d);
}


  parseErrorString=(successResponse)=>{
    let errorMessage = "";
            const keys= Object.keys(successResponse.messages)
            keys.map((key)=>{
              successResponse.messages[key].map(value=>{
                errorMessage += value+"\n"
              })
            })
            return errorMessage
  }

  onBackPress = () => {
    // this.props.navigation.pop();
    this.setState({value:''})
    this.props.navigation.goBack();
    return true;
  };


  getBookingDate() {
    const d = new Date(this.state.value.date);
    return d.toDateString();
  }
  fetchLocationById = () => {
    id = this.state.value.id;
    locationDetailsById(
      id,
      sResponse => {
        console.log(sResponse);
        const location_data = {...sResponse.location};
        let payment_modes = location_data.payment_mode.split(",").map(item => item.trim());
        this.setState({...location_data,payment_modes:payment_modes});
      },
      error => {
        alert("Unable to fetch location details. Please try again");
      },
    );
    // getTransactionDetailByID(this.state.access_token);
  };

  getTransactionDetails(){
    console.log()
    getTransactionDetailByID(this.state.access_token,this.state.value.id,
      (success)=>{
        console.log("Transaction details by id",success)
        this.setState({...success.data})
      },
      (failure)=>{
        alert('Error Getting transaction details. Please try again')
        this.props.navigation.back();

      })
  }

  componentWillUnmount() {
    this.backHandler.remove()
    Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
  }
  
  componentDidMount() {
    // console.log("payment page")
    // console.log(this.props.navigation.state.params.request_id);
    
    // console.log('this.state.access_token')
    // alert(console.log(this.state.value))
    // alert(console.log(this.state.access_token))
    // alert(console.log(this.state.value.id))
    
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
    this.fetchLocationById();
    try {
      AsyncStorage.getItem(ACCESS_TOKEN).then(token => {
        if (token === null) {
          this.props.navigation.navigate('Login');
        } else {
          this.setState({access_token: token});
          getTransactionDetailByID(token,this.props.navigation.state.params.request_id,
            (success)=>{
              if(success.status==="success")
              {
                console.log("Transaction details by id",success)
                this.setState({...success.request_info})
              }else{
                alert("Unable to get transaction details. Please try again")
              }
              
            },
            (failure)=>{
              alert('Error Getting transaction details. Please try again')
              this.props.navigation.back();
      
            })
        }
      });
    } catch (error) {
      alert("Something went wrong. Please try again in some time");
    }
  }
  // updateIndex(selectedIndex) {
  //   this.setState({selectedIndex});
  // }

  ticketPayment = () => {
    if(this.state.pressStatus === true){
      this.setState({loading: true});
      if(this.state.payment === "CASH"){
      payment(
        this.state.access_token,
        this.props.navigation.state.params.request_id,
        this.state.payment,
        success => {
          console.log(success)
          if(success.status==="success"){
          console.log("Payment",success)
          this.setState({loading: false,success:true});
          // Alert.alert('Response', JSON.stringify(success.messages));
          this.props.navigation.navigate('Ticket', {
            value: success.data,
            parking_at: this.state.value.name,
            from_time: this.state.value.from_time,
            to_time: this.state.value.to_time,
            amount:success.data.amount,
            from_time_12hr: this.state.value.from_time_12hr,
            to_time_12hr: this.state.value.to_time_12hr,
            location_id:this.state.value.id
          });
        }else if (success.status==="error"){

          this.setState({loading:false,success:false})
          this.setState({loading:false})
          Alert.alert('Response', JSON.stringify(success));
        }
        },
        error => {
          this.setState({loading: false,success:false});
          this.props.navigation.navigate('Home');
          alert(error);
        },
      );
    }else if(this.state.payment==="PAYTM"){
      console.log("paytm")
      payment(
        this.state.access_token,
        this.props.navigation.state.params.request_id,
        this.state.payment,
        success => {
          console.log("online payment",success)
          if(success.status==="success"){
          console.log("Payment",success)
          this.setState({loading: false});
          this.runTransaction(success.data)
          // Alert.alert('Response', JSON.stringify(success.messages));
          // this.props.navigation.navigate('Ticket', {
          //   value: success.data,
          //   parking_at: this.state.value.name,
          //   from_time: this.state.value.from_time,
          //   to_time: this.state.value.to_time,
          //   amount:success.data.amount,
          //   from_time_12hr: this.state.value.from_time_12hr,
          //   to_time_12hr: this.state.value.to_time_12hr,
          // });
        }else if (success.status==="error"){
          this.setState({loading:false,success:false})
          
          Alert.alert('Response', JSON.stringify(success));
        }
        },
        error => {
          this.setState({loading: false,success:false});
          this.props.navigation.navigate('Home');
          alert(error);
        },
      );
    }
    }else{
      alert("Please Select Payment Method")
    }
    
  };

  getPaymentStatus=()=>{
    if(this.state.success === true){
      if(this.state.payment==="CASH"){
      return(
        <PaymentSuccess message="Booking Successful"/>
      )}else{
        return(
          <PaymentSuccess message="Payment Successful"/>
        )
      }
    }else if(this.state.success === false){
      <PaymentError message="Error occurred while booking"/>
    }
  }

  render() {
    const radio_props = [
      {label: 'Phonepe', value: 0},
      {label: 'GPay', value: 1},
      {label: 'BHIM UPI', value: 2},
    ];
    return (
      <ScrollView>
        <StatusBar backgroundColor="#000" barStyle="light-content" />

        <View
          style={{
            marginTop: 20,
            borderWidth: 1,
            borderTopLeftRadius: 50,
            borderTopRightRadius: 50,
            borderColor: '#ddd',
            borderBottomColor: '#fff',
          }}>
          
            <View
              style={{
                width: screenWidth - 30,
                alignSelf: 'center',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 30,
                borderColor: '#ddd',
                borderWidth: 1,
                borderRadius: 50,
                height:70,
                paddingRight: 15,
                borderBottomWidth:4,
                borderRightWidth:3,
              }}>
              <Image
                source={GoogleMapImage}
                style={{
                  width: 70,
                  height: 65,
                  borderTopLeftRadius: 50,
                  borderBottomLeftRadius: 50,
                }}
              />
              <View style={{flexDirection: 'column', padding: 10}}>
                <Text
                  style={{fontFamily: 'Lato-Bold', width: screenWidth * 0.5}}>
                  {this.state.value.name}
                </Text>
                <Text
                  numberOfLines={2}
                  style={{
                    width: screenWidth * 0.5,
                    fontFamily: 'Lato-Regular',
                    textAlign: 'justify',fontSize:12
                  }}>
                  {this.state.value.address_line1}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  const data = {
                    destination: {
                      latitude: parseFloat(this.state.latitude),
                      longitude: parseFloat(this.state.longitude),
                    },
                  };
                  getDirections(data);
                }}>
                <Icon
                  name="location-arrow"
                  type="FontAwesome"
                  style={{fontSize: 30, marginTop: 18}}
                />
              </TouchableOpacity>
            </View>
            <View
            style={{flexDirection:'row',paddingLeft:20,paddingRight:20,justifyContent:'space-between'}}
            >
            <Text
            style={{
              fontSize: 16,
              color: 'gold',
              fontFamily: 'Lato-Bold',
              alignSelf: 'flex-end',
              paddingTop: 20,
              paddingRight: 15,
            }}>
            Capacity {this.state.capacity}
          </Text>

          <Text
            style={{
              fontSize: 16,
              color: 'gold',
              fontFamily: 'Lato-Bold',
              alignSelf: 'flex-end',
              paddingTop: 20,
              paddingRight: 15,
            }}>
            Available {this.state.available_slots}
            
          </Text>
          </View>
          <View
            style={{
              borderTopColor: '#ddd',
              borderTopWidth: 1,
              borderStyle: 'dashed',
              marginTop: 20,
            }}>
            <Text
              style={{
                paddingLeft: 15,
                paddingBottom: 15,
                marginTop: 25,
                fontFamily: 'Lato-Black',
              }}>
              PARKING DETAILS
            </Text>
          </View>
          <View
            style={{
              borderColor: '#aaa',
              borderWidth: 2,
              borderBottomRightRadius: 20,
              borderBottomLeftRadius: 20,
              width: screenWidth - 30,
              alignSelf: 'center',
              marginTop: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 15,
              }}>
              <Image source={Parking} style={{width: 50, height: 50}} />
              <Text
                style={{
                  fontFamily: 'Lato-Black',
                  color: '#000',
                  textAlign: 'right',
                  padding: 15,
                }}>
                On
                <Text style={{fontFamily: 'Lato-Regular', }}>
                  {' '}
                  {this.getBookingDate()}
                </Text>
              </Text>
            </View>

            <View style={{padding: 15}}>
              <Text style={{fontFamily: 'Lato-Black', }}>
                Parking at - 
                <Text > {this.state.value.name}</Text>
              </Text>
              <Text
                style={{fontFamily: 'Lato-Black', marginTop: 5, }}>
                From - 
                <Text style={{color: '#000'}}>
                  {' '}
                  {this.state.value.from_time_12hr} to {this.state.value.to_time_12hr}
                </Text>
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: 15,
                paddingRight: 15,
                paddingTop: 10,
                paddingBottom:10
              }}>
              <Text style={{width:Dimensions.get('window').width*0.7,flexWrap:'wrap' ,fontFamily: 'Lato-Black'}}>
                Parking handling Charges (inclusive of taxes) - 
              </Text>
              <Text style={{color: '#000', fontFamily: 'Lato-Bold'}}>
              {"\u20B9 "+this.state.amount}
              </Text>
            </View>
            {/* <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: 15,
                paddingRight: 15,
                paddingTop: 10,
                paddingBottom: 20,
              }}>
              <Text style={{color: '#aaa', fontFamily: 'Lato-Black'}}>Convenience Fee</Text>
              <Text style={{color: '#000', fontFamily: 'Lato-Bold'}}>
                {"\u20B9 "+this.state.convenience_fee}
              </Text>
            </View> */}
            <Divider style={{height: 2, borderStyle: 'dashed'}} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 15,
              }}>
              <Text
                style={{ fontFamily: 'Lato-Black', fontSize: 20}}>
                Total
              </Text>
              <Text
                style={{color: '#000', fontFamily: 'Lato-Bold', fontSize: 20}}>
                <Text> {'\u20B9'} </Text>
                {this.state.amount}
              </Text>
            </View>
          </View>
          {this.state.loading ? (
            <ActivityIndicator
              style={{marginTop: 10, marginBottom: 10}}
              size="small"
              color="#000000"
            />
          ) : (
            <React.Fragment />
          )}
          <Text style={{padding: 15, marginTop: 20, fontFamily: 'Lato-Black'}}>
            PAYMENT METHOD
          </Text>
          <ScrollView horizontal={true}>
            {this.state.payment_modes.includes('cash') || this.state.payment_modes.includes('free') ?
            
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  show: true,
                  show1: false,
                  show2: false,
                  show3: false,
                  payment:'CASH',
                  pressStatus:true,
                });
              }}
              style={
                this.state.pressStatus && this.state.payment==="CASH"
                    ? styles.buttonPress
                    : styles.button
              }>
              <Text
                style={{color: '#000', fontFamily: 'Lato-Bold', fontSize: 20}}>
                Cash
              </Text>
              {this.state.show && (
                <Icon
                  type="FontAwesome"
                  name="check"
                  style={{
                    color: '#000',
                    fontSize: 20,
                    position: 'absolute',
                    alignSelf: 'flex-end',
                  }}></Icon>
              )}
            </TouchableOpacity>:<React.Fragment />
            
            }
            { this.state.payment_modes.includes('online') ?

              <TouchableOpacity
              
              onPress={() => {
                this.setState({
                  show1: true,
                  show: false,
                  show2: false,
                  show3: false,
                  pressStatus:true,
                  payment:"PAYTM"
                });
              }}
              style={this.state.pressStatus && this.state.payment==="PAYTM"
                ? styles.buttonPress
                : styles.button}>
              <Text
                style={{color: '#000', fontFamily: 'Lato-Bold', fontSize: 20}}>
                Online
              </Text>
              {this.state.show1 && (
                <Icon
                  type="FontAwesome"
                  name="check"
                  style={{
                    color: '#000',
                    fontSize: 20,
                    position: 'absolute',
                    alignSelf: 'flex-end',
                  }}></Icon>
              )}
            </TouchableOpacity>:<React.Fragment />}
            {/*<TouchableOpacity 
                        disabled
                            onPress={()=>{
                                this.setState({show2:true,show:false,show1:false,show3:false})
                            }}
                            style={{backgroundColor:'#ddd',padding:20,paddingLeft:25,paddingRight:25,marginRight:15,borderRadius:10}}>
                            <Text style={{color:'#000',fontFamily:'Lato-Black'}}>DEBIT</Text>
                            {this.state.show2 && (
                                <Icon type="FontAwesome" name="check" style={{color:'#000',fontSize:20,position:'absolute',alignSelf:'flex-end'}}></Icon>
                            )}
                        </TouchableOpacity>
                        <TouchableOpacity 
                        disabled
                            onPress={()=>{
                                this.setState({show3:true,show:false,show1:false,show2:false})
                            }}
                            style={{backgroundColor:'#ddd',padding:20,paddingLeft:35,paddingRight:35,marginRight:15,borderRadius:10}}>
                            <Text style={{color:'#000',fontFamily:'Lato-Black'}}>UPI</Text>
                            {this.state.show3 && (
                                <Icon type="FontAwesome" name="check" style={{color:'#000',fontSize:20,position:'absolute',alignSelf:'flex-end'}}></Icon>
                            )}
                        </TouchableOpacity> */}
          </ScrollView>

          {/* <View style={{paddingLeft:15,marginTop:20}}>
                        <RadioForm
                            radio_props={radio_props}
                            initial={0}
                            onPress={(radioCheck) => {this.setState({radioCheck:radioCheck})}}
                            buttonWrapStyle={{marginBottom: 50}}
                            buttonColor={'#ffc73e'}
                            selectedButtonColor={'#ffc73e'}
                            labelStyle={{fontFamily:'Lato-Regular'}}
                        />
                    </View> */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
              marginBottom: 20,
              padding: 15,
            }}>
            <Text
              style={{
                width: screenWidth / 2,
                color: '#aaa',
                fontStyle: 'italic',
                marginTop: 15,
                fontFamily: 'Lato-Regular',
              }}>
              {" "}
            </Text>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: 100,
                backgroundColor: 'gold',
                borderRadius: 10,
                padding: 15,
                paddingLeft:18
              }}
              onPress={this.ticketPayment}>
              <React.Fragment>
                  <Text style={{fontFamily: 'Lato-Black',fontSize:20}}>{"Pay  "}</Text>
                  <Image
                    style={{color:"black"}}
                      source={Arrowright}
                      style={{marginLeft:5,resizeMode:'contain', width: 15, height: 15 }}
                    />
              </React.Fragment>
            </TouchableOpacity>
          </View>
          {
            this.getPaymentStatus()
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  button:{
    backgroundColor: '#ddd',
    padding: 20,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 10,
    marginRight: 15,
    borderRadius: 10,
  },
  buttonPress:{
    backgroundColor: 'gold',
    padding: 20,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 10,
    marginRight: 15,
    borderRadius: 10,
  }
});
