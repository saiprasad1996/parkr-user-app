import React from 'react'
import {
    View,
    Text,
    ScrollView,
    StatusBar,Dimensions,TouchableOpacity,
} from 'react-native';
import { Overlay } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;


export default class PaymentError extends React.Component{
    constructor(props){
        super(props)
        this.state={
            isVisible:true
        }
    }
    close = () => {
        this.setState({isVisible:false})
    }
    
    render(){
        return (
            <Overlay isVisible={this.state.isVisible} overlayBackgroundColor="transparent">
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                <View style={{alignSelf:'center',marginTop:screenHeight/4}}>
                    <View style={{backgroundColor:'#fff',alignSelf:'center',width:screenWidth-30,borderRadius:20}}>
                        <View style={{alignItems:'center',marginTop:50,marginBottom:50}}>
                            <Icon name="times-rectangle-o" type="FontAwesome" style={{fontSize:40,color:'red'}}></Icon>
                            <Text style={{marginTop:10,fontWeight:'bold',fontSize:20}}>Error...</Text>
                            <Text style={{marginTop:10,}}>Error Details</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:15}}>
                            <TouchableOpacity onPress={this.close} style={{backgroundColor:'#000',padding:8,borderRadius:20}}>
                                <Text style={{color:'#fff'}}>CLOSE</Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity style={{backgroundColor:'gold',padding:8,borderRadius:20}}>
                                <Text style={{color:'#000'}}>TRY AGAIN</Text>
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </View>
            </Overlay>
        )
    }
}

