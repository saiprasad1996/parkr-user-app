import React from 'react'
import {
    View,
    Text,
    ScrollView,
    StatusBar,Dimensions,TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import {Overlay} from 'react-native-elements';

export default class PaymentSuccess extends React.Component{
    constructor(props){
        super(props)
        this.state={
            isVisible:true,
        }
    }
    close = () => {
        this.setState({isVisible:false})
    }
    render(){
        return (
            <Overlay overlayBackgroundColor="transparent" isVisible={this.state.isVisible}
            windowBackgroundColor="rgba(0, 0, 0, 0.5)"
            >
               
                <View style={{alignSelf:'center',marginTop:screenHeight/4}}>
                    <View style={{backgroundColor:'#fff',alignSelf:'center',width:screenWidth-30,borderRadius:20}}>
                        <View style={{alignItems:'center',marginTop:50,marginBottom:50}}>
                            <Icon name="check" type="FontAwesome" style={{fontSize:80,color:'green'}}></Icon>
                            <Text style={{marginTop:10,fontSize:18,fontFamily:'Lato-Bold',fontWeight:'bold'}}>{this.props.message}</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:15}}>
                            <TouchableOpacity onPress={this.close} style={{backgroundColor:'#000',padding:10,borderRadius:20}}>
                                <Text style={{color:'#fff',fontFamily:'Lato-Black'}} >Close</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.close} style={{backgroundColor:'gold',padding:10,borderRadius:20}}>
                                <Text style={{color:'#000',fontFamily:'Lato-Black'}}>Show Ticket</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Overlay>
        )
    }
}

