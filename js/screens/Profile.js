import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
} from 'react-native';
import {Divider} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import AsyncStorage from '@react-native-community/async-storage';
import LabeledInput from '../components/inputs/LabeledInput';
import {updatePassword} from '../services/api';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldpassword: '',
      newpassword: '',
      confirm_password: '',
      access_token: '',
      loading: false,
    };
  }

  componentDidMount() {
    ToastAndroid.show('Please wait...', ToastAndroid.LONG);
    console.log('Vehicles');

    try {
      AsyncStorage.getItem('access_token').then(access_token => {
        this.setState({access_token: access_token});
      });
    } catch (error) {}
  }

  update_Password = () => {
    this.setState({loading: true});
    if (
      this.state.oldpassword === '' ||
      this.state.newpassword === '' ||
      this.state.confirm_password === ''
    ) {
      this.setState({loading: false});
      ToastAndroid.show('Please enter all the fields', ToastAndroid.LONG);
      alert('Please enter all fields');
    } else {
      if (this.state.newpassword === this.state.confirm_password) {
        updatePassword(
          this.state.access_token,
          this.state.oldpassword,
          this.state.newpassword,
          this.state.confirm_password,
          success => {
            this.setState({loading: false});
            this.props.navigation.navigate('PasswordUpdate');
          },
          error => {
            this.setState({loading: false});
            alert('Something went wrong while updating the password');
          },
        );
      }
    }
  };

  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <View style={{alignSelf: 'center', marginTop: 50}}>
          <Text
            style={{textAlign: 'left', fontWeight: 'bold', marginBottom: 20}}>
            Please Enter Your Old Password
          </Text>
          <LabeledInput
            secureTextEntry={true}
            placeholder="Enter your old password"
            label="Old Password"
            onChangeText={text => {
              this.setState({oldpassword: text});
            }}
          />
          {/* <Text style={{color:'green',marginTop:10}}>Correct</Text> */}
        </View>
        <View
          style={{
            borderBottomColor: '#aaa',
            borderBottomWidth: 1,
            borderStyle: 'dashed',
            width: screenWidth,
            marginTop: 20,
            marginBottom: 20,
          }}></View>
        <View style={{alignSelf: 'center'}}>
          <Text
            style={{textAlign: 'left', fontWeight: 'bold', marginBottom: 20}}>
            Please Enter Your New Password
          </Text>
          <LabeledInput
            secureTextEntry={true}
            style={{marginBottom: 30}}
            placeholder="New password"
            label="New Password"
            onChangeText={text => {
              this.setState({newpassword: text});
            }}
          />
          <LabeledInput
            secureTextEntry={true}
            onChangeText={text => {
              this.setState({confirm_password: text});
            }}
            placeholder="Repeat password"
            label="Repeat Password"
          />
          {this.state.loading ? (
            <ActivityIndicator
              style={{marginTop: 10, marginBottom: 10}}
              size="small"
              color="#000000"
            />
          ) : (
            <React.Fragment />
          )}

          <TouchableOpacity
            onPress={this.update_Password}
            style={{
              backgroundColor: 'gold',
              width: screenWidth / 2,
              alignSelf: 'center',
              alignItems: 'center',
              padding: 13,
              borderRadius: 10,
              marginTop: 50,
            }}>
            <Text style={{fontFamily:'Lato-Bold',fontSize:18}}>Change Password</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
