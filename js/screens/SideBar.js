import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Linking,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Divider} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../assets/images/images.png';
import LabeledInput from '../components/inputs/LabeledInput';
import AsyncStorage from '@react-native-community/async-storage';
import {ACCESS_TOKEN} from '../services/constants';
import {withNavigation} from 'react-navigation';
import {connect} from 'react-redux';
import {LOGOUT} from '../store/constants';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      // access_token:""
    };
  }


  openExternalURL(url) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          ToastAndroid.show("Can't handle url: " + url, ToastAndroid.LONG);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  componentDidMount() {
    this.refreshScreen();
    const didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        console.debug('didBlur', payload);
      },
    );
    didBlurSubscription.remove();
  }

  refreshScreen() {
    try {
      AsyncStorage.getItem('phone').then(phone => {
        if (phone === null) {
          this.setState({phone: null});
        } else {
          this.setState({phone: phone});
        }
      });
    } catch (error) {}
  }

  openExternalURL(url) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          ToastAndroid.show("Can't handle url: " + url, ToastAndroid.LONG);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }
  renderWithoutLogin() {
    return (
      <View style={styles.menubox}>
        <View style={{flexDirection: 'row', width: 100}}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Login');
            }}
            style={{
              flexDirection: 'row',
              marginTop: 8,
              marginBottom: 8,
              backgroundColor: '#ffc73e',
              paddingLeft: 20,
              borderRadius: 20,
              width: 100,
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
                padding: 10,
                fontFamily: 'Lato-Bold',
              }}>
              Login
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Signup');
            }}
            style={{
              flexDirection: 'row',
              marginTop: 8,
              marginLeft: 25,
              marginBottom: 8,
              paddingLeft: 15,
              borderRadius: 20,
              backgroundColor: '#000000',
              width: 100,
            }}>
            <Text
              style={{
                textAlign: 'center',
                color: '#ffffff',
                fontSize: 18,
                padding: 10,
                fontFamily: 'Lato-Bold',
              }}>
              Signup
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            bottom: 0,
            marginTop: screenHeight * 0.55,
          }}>
          <TouchableOpacity
          onPress={() => {
            this.openExternalURL(
              'http://booking.parkr.in/help'
            );
          }}
          >
            <Text style={{fontWeight: 'bold',alignSelf:'stretch',textAlign:'center', color: '#aaa'}}>Help</Text>
          </TouchableOpacity>
          <TouchableOpacity
          onPress={() => {
            this.openExternalURL(
              'http://booking.parkr.in/terms-and-conditions',
            );
          }}
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              bottom: 0,
            }}>
            <Text style={{color: '#aaa'}}>Terms V 0.1 </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <View style={{flexDirection: 'row', backgroundColor: '#f1f1f1'}}>
          <View
            style={{
              paddingTop: 25,
              paddingBottom: 20,
              paddingLeft: 20,
              backgroundColor: '#f1f1f1',
            }}>
            <Image
              source={Logo}
              style={{
                width: 60,
                height: 60,
                borderRadius: 150 / 2,
                overflow: 'hidden',
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginLeft: 15,
              paddingTop: 35,
              paddingBottom: 20,
            }}>
            <Text
              style={{
                color: '#000',
                backgroundColor: 'transparent',
                padding: 4,
                textAlign: 'left',
                paddingTop: 10,
                fontFamily: 'Lato-Bold',
                fontSize: 15,
              }}>
              {this.props.phone === null
                ? ' Welcome, Guest'
                : ' Welcome'}
            </Text>
            <Text
              style={{
                color: '#000',
                backgroundColor: 'transparent',
                padding: 4,
                textAlign: 'left',
                fontSize: 12,
              }}>
              {this.props.phone}
            </Text>
          </View>
        </View>
        <Divider />
        <View style={{borderWidth: 0, flex: 1, backgroundColor: 'white'}}>
          {this.props.phone !== null ? (
            <View style={styles.menubox}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Home');
                }}
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 8,
                  marginBottom: 8,
                }}>
                <Icon
                  name="home"
                  type="FontAwesome"
                  style={{fontSize: 17, color: '#aaa', marginTop: 10}}
                />
                <Text style={styles.sectionHeadingStyle}>Home</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Bookings')}
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 8,
                  marginBottom: 8,
                }}>
                <Icon
                  name="history"
                  type="FontAwesome"
                  style={{fontSize: 17, color: '#aaa', marginTop: 8}}
                /><Text style={styles.sectionHeadingStyle}>My Bookings</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 6,
                  marginBottom: 8,
                }}
                onPress={() => this.props.navigation.navigate('Vehicles')}>
                <Icon
                  name="car"
                  type="FontAwesome"
                  style={{fontSize: 17, color: '#aaa', marginTop: 8}}
                /><Text style={styles.sectionHeadingStyle}>My Vehicles</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Profile');
                }}
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 6,
                  marginBottom: 8,
                }}>
                <Icon
                  name="user"
                  type="FontAwesome"
                  style={{fontSize: 17, color: '#aaa', marginTop: 8}}
                />
                <Text style={styles.sectionHeadingStyle}>Profile</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={event => {
                  AsyncStorage.multiRemove([ACCESS_TOKEN, 'phone']).then(
                    returnval => {
                      console.log(returnval);
                      this.props.logout()
                      this.props.navigation.navigate('HomeDrawer');
                      
                      
                    },
                  );
                }}
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 6,
                  marginBottom: 8,
                }}>
                <Icon
                  name="power-off"
                  type="FontAwesome"
                  style={{fontSize: 17, color: '#aaa', marginTop: 8}}
                />
                <Text style={styles.sectionHeadingStyle}>Logout</Text>
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  bottom: 0,
                  marginTop: screenHeight * 0.3,
                }}>
                <TouchableOpacity
                onPress={() => {
                  this.openExternalURL(
                    'http://booking.parkr.in/help'
                  );
                }}
                
                >
                  <Text style={{fontWeight: 'bold', color: '#aaa'}}>Help</Text>
                </TouchableOpacity>
                <TouchableOpacity

                  onPress={() => {
                    this.openExternalURL(
                      'http://booking.parkr.in/terms-and-conditions',
                    );
                  }}

                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    bottom: 0,
                  }}>
                  <Text style={{color: '#aaa'}}>Terms V 0.1 </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            this.renderWithoutLogin()
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  menubox: {
    flex: 1,
    padding: 25,
  },
  sectionHeadingStyle: {
    flex: 1,
    color: '#000',
    fontSize: 13,
    paddingTop: 8,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    fontFamily: 'Lato-Bold',
  },
});

const SideBarNav = withNavigation(SideBar);

const mapPropsToState = store => {
  return {
    token: store.access_token,
    phone: store.phone,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch({type: LOGOUT}),
  };
};

export default connect(mapPropsToState, mapDispatchToProps)(SideBarNav);
