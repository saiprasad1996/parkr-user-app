import React from 'react'
import SideBar from './SideBar'

export default class SidebarContainer extends React.Component{
    render(){
        return (
            <SideBar />
        )
    }
}