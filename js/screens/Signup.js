import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import {Button, Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../assets/images/logo.png';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import LabeledInput,{PasswordLabeledInput} from '../components/inputs/LabeledInput';


import { LoginManager,AccessToken,GraphRequestManager,GraphRequest } from 'react-native-fbsdk'
import { GoogleSignin, statusCodes } from 'react-native-google-signin';

import {register, userExists} from '../services/api'


/* const data = {'name':'NiharikaTest','email':'niharika@gmail.com','mobile_number':'9837124762','password':'test@niharika'};
	fetch('http://api.stage.parkr.in/users/register', { 	
		method: 'POST', 
		headers: { 'Content-Type': 'application/json', },
		body: JSON.stringify(data), }) 
		.then((response) => response.json()) 
		.then((data) => { console.log('Success:', data); }) 
		.catch((error) => { console.error('Error:', error); }); */
export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      mobile_no: '',
      password: '',
      password_again: '',
      loading: false,
    };
  }

  populateFields(name,email){
    this.setState({name:name,email:email})
  }

  componentDidMount(){
    GoogleSignin.configure({
      scopes: [], webClientId: '883055851534-tqbj1qok3vphj9qd4es0up5m74bu2msj.apps.googleusercontent.com', 
      offlineAccess: true, 
      hostedDomain: '', 
      loginHint: '', 
      forceConsentPrompt: true, 
      accountName: '',
      androidClientId:'883055851534-69hna36epromi4esai797odt7a82v0tl.apps.googleusercontent.com',
      iosClientId: 'XXXXXX-krv1hjXXXXXXp51pisuc1104q5XXXXXXe.apps.googleusercontent.com'
      });
  }


  handleGoogleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo)
      console.log(userInfo.user.email)
      await userExists(userInfo.user.email,
        (success)=>{
          console.log(success)
          if(success.status==="error"){
            alert("User with same email already exists")
            this.setState({loading:false})
      
          }else{
            this.populateFields(userInfo.user.name,userInfo.user.email)
      
            this.setState({loading:false})
          }
            
        },(error)=>{
          alert('There was some error logging you in. Please contact developer '+error)
          this.setState({loading:false})
        })
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('user cancelled')
        alert("Login Cancelled")
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('Login in progress')
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('Play services outdated')
        alert('Google Play services not available')
      } else {
        // some other error happened
        console.log(error)
      }
    }
  };
  
  handleFacebookLogin =()=> {
    this.setState({loading:true})
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
       (result)=> {
        if (result.isCancelled) {
          alert('Login cancelled')
          this.setState({loading:false})
        } else {
          console.log('Login result: ' , result)
          console.log('Login success with permissions: ' + result.grantedPermissions.toString())
          let accessToken;
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              accessToken = data.accessToken
              console.log(accessToken.toString())
            }
          )

          const responseInfoCallback = (error, result) => {
            if (error) {
              console.log(error)
              alert('Error fetching data: ' + error.toString());
              this.setState({loading:false})
            } else {
              console.log(result)
              // alert('Success fetching data: ' + JSON.stringify(result));
              userExists(result.email,
                (success)=>{
                  console.log(success)
                  if(!success.status==="error"){
                    alert("User with same email already exists")
                    this.setState({loading:false})
              
                  }else{
                    this.populateFields(result.name,result.email)
              
                    this.setState({loading:false})
                  }
                    
                },(error)=>{
                  alert('There was some error logging you in. Please contact developer '+error)
                  this.setState({loading:false})
                }) 
              
        
            }
          }
          
          const infoRequest = new GraphRequest(
            '/me',
            {
              accessToken: accessToken,
              parameters: {
                fields: {
                  string: 'email,name,first_name,middle_name,last_name'
                }
              }
            },
            responseInfoCallback
          );

          // Start the graph request.
          new GraphRequestManager().addRequest(infoRequest).start()
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error)
        this.setState({loading:false})
      }
    )
    
  }



  register = () => {
    this.setState({loading: true});
    const name = this.state.name.trim();
    const email = this.state.email.trim();
    const mobile_no = this.state.mobile_no.trim();
    const password = this.state.password.trim();
    const password_again = this.state.password_again.trim();
    if(name.length <3){
      ToastAndroid.show('Your name should atleast be of 3 letters', ToastAndroid.LONG);
      this.setState({loading:false})
      return 
    }
    if (name === '' || email === '' || mobile_no === '' || password === '') {
      ToastAndroid.show('Please enter all the details', ToastAndroid.LONG);
      this.setState({loading:false})
      return 
    } else if (password === password_again) {
      register(
        name,
        email,
        mobile_no,
        password,
        successResponse => {
          this.setState({loading: false});
          console.log(successResponse);
          if (successResponse.status === 'success') {
            ToastAndroid.show('Registration success', ToastAndroid.LONG);
            this.props.navigation.goBack();
          } else {
            let errorMessage = "";
            const keys= Object.keys(successResponse.messages)
            keys.map((key)=>{
              successResponse.messages[key].map(value=>{
                errorMessage += value+"\n"
              })
            })
            alert(errorMessage)
            // ToastAndroid.show(`Registration failed. Report code - ${JSON.stringify(successResponse)}`, ToastAndroid.LONG);
            console.log(successResponse)
          }
          console.log(successResponse);
        },
        errorResponse => {
          this.setState({loading: false});
          ToastAndroid.show(`Oops Something went wrong. Report Code - ${JSON.stringify(errorResponse)}`, ToastAndroid.LONG);
          console.log(errorResponse);
        },
      );
    } else {
      ToastAndroid.show('Password and confirm password should be same', ToastAndroid.LONG);
    }
  };

  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <View style={styles.container}>
          <Image source={Logo} style={styles.logo} />
          <Text style={{color: '#b0b0b0', fontSize: 18}}>Sign up with</Text>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Button
              icon={
                <Icon
                  name="google"
                  style={{marginRight: 10}}
                  size={15}
                  color="#ff3a00"
                />
              }
              raised
              type="outline"
              title="Google"
              onPress={()=>{
                this.handleGoogleLogin()
              }}
              titleStyle={{color: 'black'}}
              buttonStyle={{borderColor: '#e2e2e2'}}
              containerStyle={{marginTop: 20, marginRight: 20, width: 120}}
            />

            <Button
              icon={
                <Icon
                  name="facebook-square"
                  style={{marginRight: 10}}
                  size={20}
                  color="#385ea5"
                />
              }
              raised
              type="outline"
              onPress={()=>{
                this.handleFacebookLogin()
              }}
              title="Facebook"
              titleStyle={{color: 'black'}}
              buttonStyle={{borderColor: '#e2e2e2'}}
              containerStyle={{marginTop: 20, width: 120}}
            />
          </View>
          <View style={styles.lineStyle} />

          <LabeledInput
            placeholder="Enter Name"
            label="Name"
            style={{marginTop: 20}}
            iconName="user-circle-o"
            value={this.state.name}
            onChangeText={text => {
              this.setState({name: text});
            }}
          />
          <LabeledInput
            placeholder="Enter Email id"
            label="Email"
            value={this.state.email}
            style={{marginTop: 20}}
            iconName="envelope-square"
            onChangeText={text => {
              this.setState({email: text});
            }}
          />
          <LabeledInput
            placeholder="Enter Phone No."
            label="Mobile No"
            style={{marginTop: 20}}
            keyboardType="phone-pad"
            iconName="mobile-phone"
            onChangeText={text => {
              this.setState({mobile_no: text});
            }}
          />
          <PasswordLabeledInput
            placeholder="Type password"
            label="Password"
            style={{marginTop: 20}}
            iconName={'lock'}
            secureTextEntry={true}
            onChangeText={text => {
              this.setState({password: text});
            }}
          />

          <PasswordLabeledInput
            placeholder="Type password again"
            label="Confirm Password"
            style={{marginTop: 20}}
            secureTextEntry={true}
            iconName={'lock'}
            onChangeText={text => {
              this.setState({password_again: text});
            }}
          />
          {this.state.loading ? (
            <ActivityIndicator
              style={{marginTop: 10, marginBottom: 10}}
              size="small"
              color="#000000"
            />
          ) : (
            <React.Fragment />
          )}
          <Button
            title="Signup"
            raised
            onPress={this.register}
            titleStyle={{color: 'black', color: '#fff', fontWeight: 'bold'}}
            buttonStyle={{
              borderColor: '#ffc73e',
              backgroundColor: '#000000',
              borderRadius: 6,
            }}
            containerStyle={{
              marginTop: 25,
              width: Dimensions.get('window').width - 50,
            }}
          />
          <View style={{flexDirection: 'row', marginTop: 20}}>
            <TouchableOpacity>
              <Text style={{color: '#6B6B6B'}}>Having Issues?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 200,
    marginTop: 20,
    resizeMode: 'contain',
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#dbdbdb',
    margin: 10,
    marginTop: 30,
    marginBottom: 30,
    width: Dimensions.get('window').width - 50,
  },
  loginButton: {
    backgroundColor: '#ffc73e',
  },
});
