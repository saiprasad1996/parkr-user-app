import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image,
    
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import logo from '../assets/images/logo.png'
import {StackActions,NavigationActions} from 'react-navigation'
import {connect} from 'react-redux'
import { LOGIN } from '../store/constants'

class Splash extends React.Component{
    
  constructor(props){
    super(props)
    
  }

  async retriveCredentials() {
    try {
      const access_token =  await AsyncStorage.getItem("access_token");
      const phone =  await AsyncStorage.getItem("phone");
      console.log(access_token,phone)
      this.props.login(access_token,phone)
      
    } catch (error) {
      console.log(error.message);
    }
    return
  }
    componentDidMount(){
      try{
       
        this.retriveCredentials()
        
      }
      catch(error){

      }
        setTimeout(()=>{
          const resetAction  = StackActions.reset({
            index:0,
            actions:[NavigationActions.navigate({routeName:'HomeDrawer'})]
          })
          this.props.navigation.dispatch(resetAction)
        },1000)
    }
    render(){
        return (
            <View style={styles.container}>
                <Image source={logo} style={styles.logo} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'center'
    },
    logo: {
        justifyContent:"center",
      width: Dimensions.get('window').width-100,
      alignItems:'center',
      resizeMode: 'contain',
    },
    lineStyle: {
      borderWidth: 0.5,
      borderColor: '#dbdbdb',
      margin: 10,
      marginTop: 30,
      marginBottom: 30,
      width: Dimensions.get('window').width - 50,
    },
    loginButton: {
      backgroundColor: '#ffc73e',
    },
  });
  


const mapPropsToState = store => {
  return {
    token: store.access_token,
    phone: store.phone,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: (access_token,phone) => dispatch({type: LOGIN,payload:{access_token:access_token,phone:phone}})
  };
};

export default connect(mapPropsToState, mapDispatchToProps)(Splash);