import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Alert,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ToastAndroid,
  Share,
  ActivityIndicator,
  Linking,
  BackHandler,
} from 'react-native';

import getDirections from 'react-native-google-maps-directions';
import Icon from 'react-native-vector-icons/FontAwesome';
import GoogleMapImage from '../assets/images/googlemap.jpg';
import {Divider, Button, Overlay} from 'react-native-elements';
const screenWidth = Dimensions.get('window').width;
import QR from '../assets/images/qr.png';
import {cancelBooking, locationDetailsById} from '../services/api';
const screenHeight = Dimensions.get('window').height;
import {connect} from 'react-redux';
import {baseurl} from '../services/api';

class Ticket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.navigation.state.params.value,
      parking_at: this.props.navigation.state.params.parking_at,
      from_time: this.props.navigation.state.params.from_time,
      to_time: this.props.navigation.state.params.to_time,
      qrcode: this.props.navigation.state.params.qrcode,
      amount: this.props.navigation.state.params.amount,
      ticket_overlay: false,
      selected_qr: '',
      loading: false,
      location_address: '',
    };
  }
  showQR(uri) {
    this.setState({
      selected_qr: uri,
      ticket_overlay: true,
    });
  }
  handleBackButtonPressAndroid = () => {
    if (this.props.navigation.state.params.from_bookings === true) {
      this.props.navigation.goBack();
      console.log('To bookings');
    } else {
      this.props.navigation.popToTop();
    }
  };
  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonPressAndroid,
    );
    locationDetailsById(
      this.props.navigation.state.params.location_id,
      sResponse => {
        console.log(sResponse);
        const location_data = {...sResponse.location};
        this.setState({...location_data});
        this.setState({closes_in: this.state.closing_time});
      },
      error => {
        alert('Unable to fetch location details. Please try again');
      },
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonPressAndroid,
    );
  }

  onShare = async () => {
    try {
      const qr =
        this.state.value != ''
          ? baseurl + this.state.value.qrcode
          : baseurl + this.state.qrcode;

      let share_str = `
Following is your booking details \n
From - ${this.state.from_time} to ${this.state.to_time}\n
On - ${this.state.value.start_date_time}\n
Payment Receipt ID - ${this.state.value.booking_id}\n
QR - ${qr}
        `;
      const result = await Share.share({
        message: share_str,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  onCancel = () => {
    this.setState({loading: true});
    cancelBooking(
      this.props.token,
      this.props.navigation.state.params.request_id,
      success => {
        console.log(success);

        if (success.status === 'success') {
          alert(success.message);
        } else if (success.status === 'error') {
          alert(success.message);
        }
        this.setState({loading: false});
      },
    );
  };

  openExternalURL(url) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          ToastAndroid.show("Can't handle url: " + url, ToastAndroid.LONG);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  render() {
    return (
      <ScrollView style={{backgroundColor: '#efedef'}}>
        <StatusBar backgroundColor="#000" barStyle="light-content" />

        <Overlay
          isVisible={this.state.ticket_overlay}
          onBackdropPress={() => this.setState({searchPressOverlay: false})}
          windowBackgroundColor="rgba(0, 0, 0, 0.5)"
          overlayBackgroundColor="#fff"
          width={Dimensions.get('window').width * 0.8}
          height="auto">
          <View style={{resizeMode: 'stretch'}}>
            <Image
              source={{
                uri: this.state.selected_qr,
              }}
              style={{
                resizeMode: 'stretch',
                width: Dimensions.get('window').width * 0.75,
                height: 300,
              }}
            />
            <Button
              title="Close"
              buttonStyle={{
                backgroundColor: '#000',
                padding: 10,
                borderRadius: 5,
              }}
              onPress={() => {
                this.setState({ticket_overlay: false, selecte_qr: ''});
              }}
            />
          </View>
        </Overlay>
        <TouchableOpacity
          onPress={() => {
            const data = {
              destination: {
                latitude: parseFloat(this.state.latitude),
                longitude: parseFloat(this.state.longitude),
              },
            };
            getDirections(data);
          }}>
          <View
            style={{
              width: screenWidth - 30,
              alignSelf: 'center',
              backgroundColor: '#ffffff',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 30,
              borderColor: '#ddd',
              borderWidth: 1,
              borderRadius: 50,
              height: 70,
              paddingRight: 15,
              borderBottomWidth: 4,
              borderRightWidth: 3,
            }}>
            <Image
              source={GoogleMapImage}
              style={{
                width: 70,
                height: 65,
                borderTopLeftRadius: 50,
                borderBottomLeftRadius: 50,
              }}
            />
            <View style={{flexDirection: 'column', padding: 10}}>
              <Text style={{fontFamily: 'Lato-Bold', width: screenWidth * 0.5}}>
                {this.state.parking_at}
              </Text>
              <Text
                numberOfLines={2}
                style={{
                  width: screenWidth * 0.5,
                  fontFamily: 'Lato-Regular',
                  textAlign: 'justify',
                  fontSize: 12,
                }}>
                {this.state.address_line1}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                const data = {
                  destination: {
                    latitude: parseFloat(this.state.latitude),
                    longitude: parseFloat(this.state.longitude),
                  },
                };
                getDirections(data);
              }}>
              <Icon
                name="location-arrow"
                type="FontAwesome"
                style={{fontSize: 30, marginTop: 18}}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>

        <View
          style={{
            borderColor: '#aaa',
            borderWidth: 2,
            borderRadius: 20,
            width: screenWidth - 30,
            alignSelf: 'center',
            marginTop: 40,
            backgroundColor: '#fff',
          }}>
          <View style={{padding: 15}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1, justifyContent: 'flex-start'}}>
                <Text style={{fontFamily: 'Lato-Black', marginBottom: 10}}>
                  Booking made on
                </Text>
              </View>
              <View style={{justifyContent: 'flex-end'}}>
                <Text
                  style={{
                    color: '#000',
                    fontFamily: 'Lato-Bold',
                    marginBottom: 10,
                  }}>
                  {this.state.value.created_at}
                </Text>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1, justifyContent: 'flex-start'}}>
                <Text style={{fontFamily: 'Lato-Black', marginBottom: 10}}>
                  Payment method used
                </Text>
              </View>
              <View style={{justifyContent: 'flex-end'}}>
                <Text
                  style={{
                    color: '#000',
                    fontFamily: 'Lato-Bold',
                    marginBottom: 10,
                  }}>
                  {this.state.value.payment_mode}
                </Text>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1, justifyContent: 'flex-start'}}>
                <Text style={{fontFamily: 'Lato-Black', marginBottom: 10}}>
                  Payment Receipt ID is
                </Text>
              </View>
              <View style={{justifyContent: 'flex-end'}}>
                <Text
                  style={{
                    color: '#000',
                    fontFamily: 'Lato-Bold',
                    marginBottom: 10,
                  }}>
                  {this.state.value.booking_id}
                </Text>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1, justifyContent: 'flex-start'}}>
                <Text style={{fontFamily: 'Lato-Black', marginBottom: 10}}>
                  Payment status was
                </Text>
              </View>
              <View style={{justifyContent: 'flex-end'}}>
                <Text
                  style={{
                    color: 'green',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    fontFamily: 'Lato-Bold',
                  }}>
                  {this.state.value.payment_status}
                </Text>
              </View>
            </View>
          </View>
          <Divider height={2} />
          <View>
            <Text
              style={{
                fontFamily: 'Lato-Black',
                padding: 15,
                color: '#000',
                textAlign: 'right',
              }}>
              On -
              <Text style={{color: '#000', fontFamily: 'Lato-Bold'}}>
                {' '}
                {this.state.value.start_date_time}
              </Text>
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            {this.state.value != '' ? (
              <TouchableOpacity
                onPress={event => {
                  this.showQR(baseurl + this.state.value.qrcode);
                }}>
                <Image
                  source={{
                    uri: baseurl + this.state.value.qrcode,
                  }}
                  style={{width: 100, height: 100}}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={event => {
                  this.showQR(baseurl + this.state.qrcode);
                }}>
                <Image
                  source={{
                    uri: baseurl + this.state.qrcode,
                  }}
                  style={{width: 100, height: 100}}
                />
              </TouchableOpacity>
            )}
            <View style={{padding: 15, paddingLeft: 0}}>
              <Text
                numberOfLines={2}
                style={{
                  fontFamily: 'Lato-Black',
                  color: '#000',
                  width: screenWidth / 2,
                }}>
                Parking at
                <Text style={{fontFamily: 'Lato-Bold', color: '#000'}}>
                  {' '}
                  {this.state.parking_at}{' '}
                </Text>
              </Text>
              <Text
                numberOfLines={2}
                style={{
                  fontFamily: 'Lato-Black',
                  marginTop: 5,
                  color: '#000',
                  width: screenWidth / 2,
                }}>
                From
                <Text style={{color: '#000', fontFamily: 'Lato-Bold'}}>
                  {' '}
                  {this.state.from_time} to {this.state.to_time}
                </Text>
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingLeft: 15,
              paddingRight: 15,
              paddingTop: 10,
              paddingBottom: 20,
            }}>
            <Text style={{color: '#000', fontFamily: 'Lato-Black'}}>
              Parking handling fees (including taxes)
            </Text>
            <Text style={{color: '#000', fontFamily: 'Lato-Bold'}}>
              {this.state.amount}
            </Text>
          </View>
          {/* <View style={{flexDirection:'row',justifyContent:'space-between',paddingLeft:15,paddingRight:15,paddingTop:10,paddingBottom:20}}>
                            <Text style={{color:'#aaa',fontFamily:'Lato-Black'}}>Tax</Text>
                            <Text style={{color:'#000',fontFamily:'Lato-Bold'}}>{parseFloat(this.state.amount)*0.18}</Text>
                        </View> */}
          <Divider style={{height: 2, borderStyle: 'dashed'}} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 15,
            }}>
            <Text
              style={{color: '#000', fontFamily: 'Lato-Black', fontSize: 20}}>
              Total
            </Text>
            <Text
              style={{color: '#000', fontFamily: 'Lato-Bold', fontSize: 20}}>
              <Text> {'\u20B9'} </Text>
              {this.state.amount}
            </Text>
          </View>
        </View>
        {this.state.loading ? (
          <ActivityIndicator
            style={{marginTop: 10, marginBottom: 10}}
            size="small"
            color="#000000"
          />
        ) : (
          <React.Fragment />
        )}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 15,
            paddingBottom: 0,
            marginTop: 20,
          }}>
          {this.props.navigation.state.params.from_bookings === true ? (
            <TouchableOpacity
              onPress={this.onCancel}
              style={{
                backgroundColor: '#000',
                padding: 10,
                borderRadius: 20,
                width: 80,
                alignItems: 'center',
              }}>
              <Text style={{color: '#fff', fontWeight: 'bold'}}>Cancel</Text>
            </TouchableOpacity>
          ) : (
            <React.Fragment />
          )}
          <TouchableOpacity
            onPress={() => {
              const data = {
                destination: {
                  latitude: parseFloat(this.state.latitude),
                  longitude: parseFloat(this.state.longitude),
                },
              };
              getDirections(data);
            }}
            style={{
              backgroundColor: '#000',
              padding: 10,
              borderRadius: 20,
              width: 80,
              marginRight: 20,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                width: 100,
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              Navigate
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.onShare}
            style={{
              backgroundColor: '#ffc73e',
              padding: 10,
              borderRadius: 20,
              width: 80,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#000',
                width: 100,
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              Share
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            padding: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => {
              this.openExternalURL('http://booking.parkr.in/refund');
            }}
            style={{
              backgroundColor: '#ddd',
              padding: 10,
              borderRadius: 20,
              marginTop: 15,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#000',
                fontWeight: 'bold',
                width: 100,
                alignSelf: 'stretch',
                textAlign: 'center',
              }}>
              Refund Policy
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.openExternalURL(
                'http://booking.parkr.in/terms-and-conditions',
              );
            }}
            style={{
              backgroundColor: '#ddd',
              padding: 10,
              borderRadius: 20,
              marginTop: 15,
              alignItems: 'center',
              alignSelf: 'stretch',
              textAlign: 'center',
            }}>
            <Text style={{fontWeight: 'bold', color: '#000'}}>T & C</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.openExternalURL('http://booking.parkr.in/help');
            }}
            style={{
              backgroundColor: '#ddd',
              padding: 10,
              borderRadius: 20,
              width: 100,
              marginTop: 15,
              alignItems: 'center',
              textAlign: 'center',
              alignSelf: 'center',
            }}>
            <Text
              style={{
                color: '#000',
                fontWeight: 'bold',
                width: 100,
                textAlign: 'center',
              }}>
              Help
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({});

const mapPropsToState = store => {
  return {
    token: store.access_token,
    phone: store.phone,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch({type: LOGOUT}),
  };
};

export default connect(mapPropsToState, mapDispatchToProps)(Ticket);
