import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Dimensions,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import {Button,Icon} from 'react-native-elements'

import AsyncStorage from '@react-native-community/async-storage';
import VehicleCard from '../components/VehicleCard';
import {locationDetailsById, userVehicles,userRemoveVehicle} from '../services/api';
import {withNavigationFocus} from 'react-navigation';
import { NavigationEvents } from 'react-navigation';

class Vehicles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      access_token:null,
      vehicles: [
       
      ],
    };
  }
  componentDidMount() {
    ToastAndroid.show('Please wait...', ToastAndroid.LONG);
    console.log("Vehicles")
    
    try {
      AsyncStorage.getItem('access_token').then(access_token => {
        this.setState({access_token:access_token})
        console.log(access_token)
        if (access_token === null) {
          this.props.navigation.navigate('Login');
        } else {
          userVehicles(
            access_token,
              response => {
                if(response.status == "error"){
                  console.log(response.message);
                }else{
                  console.log('User vehicles', response);
                  this.setState({vehicles:response.vehicles})
                }
            },
            error => {
              console.log(error);
            },
          );
        }
      });
    } catch (error) {}

  }

  removeVehicle=(vehicle_id)=>{
    userRemoveVehicle(vehicle_id,this.state.access_token,(success)=>{
      console.log(success)
      if (success.status==="success"){
       
        alert("Vehicle removed successfully")

       this.props.navigation.goBack()


      }else{
        alert('There was some problem while deleting')
      }
    },(error)=>{
      console.log(error);
      ToastAndroid.show("Internal error occurred. Please restart the app",ToastAndroid.LONG)
    })
  }

  renderBlank() {
    return (
      <View
        style={{
          flex: 1,
          padding: 30,
          backgroundColor: '#f0f0f0',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 25, textAlign: 'center', fontStyle: 'italic'}}>
          You don't have any Vehicles registered yet.
        </Text>
        <Button
              title="Add Vehicle"
              
              raised
              onPress={() => {
                this.props.navigation.navigate('AddVehicle');
              }}
              titleStyle={{color: 'black', fontWeight: 'bold'}}
              buttonStyle={{
                borderColor: '#ffc73e',
                backgroundColor: '#ffc73e',
                borderRadius: 6,
              }}
              containerStyle={{
                marginTop: 60,
                width: Dimensions.get('window').width - 50,
              }}
            />
      </View>
    );
  }

  render() {
    if (this.state.vehicles!==0) {
      return (
        <View style={styles.container}>
          <NavigationEvents
                onDidFocus={() => {
                  userVehicles(
                    this.state.access_token,
                      response => {
                        if(response.status == "error"){
                          console.log(response.message);
                        }else{
                          console.log('User vehicles', response);
                          this.setState({vehicles:response.vehicles})
                        }
                    },
                    error => {
                      console.log(error);
                    },
                  );
                }}
                />
          <View style={{flexDirection: 'row',justifyContent:'space-between', padding: 15,width:Dimensions.get('window').width-20}}>
            <Text style={{color: '#2f2f2f', fontWeight: 'bold', fontSize: 20}}>
              All your vehicles
            </Text>
            <View style={{flexDirection:'column'}}>
            <TouchableOpacity style={{flexDirection:'row'}}
            onPress={()=>{
              this.props.navigation.navigate('AddVehicle')
            }}
            >
              <Icon name='add' style={{marginTop:10}}/>
              <Text
                style={{
                  color: '#6B6B6B',
                  marginTop: 4,
                  
                }}>
                Add new Vehicle
              </Text> 
             
            </TouchableOpacity>
            </View>

          </View>
          <ScrollView contentContainerStyle={{alignItems: 'center',paddingBottom:20,paddingTop:20}}>
            {this.state.vehicles.map((car, index) => {
              return (
                <TouchableOpacity activeOpacity={0.9} key={index}>
                  <VehicleCard
                    vehicleType={car.vehicle_type_id}
                    key={index}
                    registration_number={car.registration_number}
                    
                    deleteVehicle={()=>{
                      this.removeVehicle(car.id)
                    }}
                    model={car.model}
                  />
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      );
    } else {
      return this.renderBlank();
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    
  },
});

export default withNavigationFocus(Vehicles)