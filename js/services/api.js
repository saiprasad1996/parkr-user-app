import { GOOGLE_CLIENT_ID } from "./constants";

const baseurl = "https://api.parkr.in";

/**
 * fetchApi calls the api and communcates data using callbacks
 * Returns null when get request has a body
 * @param {string} url - URL to which fetch api 
 * @param {string} headers - Headers
 * @param {string} method - get | post | put | delete 
 * @param {object} body - API call using body
 * @param {function} successCallback - Callback upon successful api fetch
 * @param {function} failureCallback - Callback upon failed api fetch
 * @param {function} errorCallback - Callback upon fetch exception
 * 
 */
const fetchApi = (url, headers, method, body = null, successCallback, errorCallback) => {
    let options = null;

    if (method === "get" && body !== null) {
        errorCallback("GET request does not support body")
        return null
    } else if (method === "get") {

        options = {
            method: method,
            headers: headers
        }
    } else {
        options = {
            method: method,
            body: JSON.stringify(body),
            headers: headers
        }
    }
    fetch(url, options)
        .then(response => response.json())
        .then(responseJson => {
            successCallback(responseJson)
        }).catch(error => {
            errorCallback(error)
        })

}



const authenticate = (username, password, successCallback, errorCallback) => {
    const data = { 'mobile_number': username, 'password': password };
    fetch(`${baseurl}/users/authenticate`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const register = (name,email,mobile_no, password, successCallback, errorCallback) => {
    const data = {'name':name,'email':email,'mobile_number':mobile_no,'password':password};
    fetch(`${baseurl}/users/register`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const getOtp = (mobile_no, successCallback, errorCallback) => {
    const data = {'mobile_number':mobile_no};
    fetch(`${baseurl}/otp/get`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}




const verifyOtp = (mobile_no,otp, successCallback, errorCallback) => {
    const data = {'mobile_number':mobile_no,'otp':otp};
    fetch(`${baseurl}/otp/verify`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const userAccount = (successCallback, errorCallback) => {
    
    fetch(`${baseurl}/users/account`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const updatePassword = (access_token,oldPassword, password,passwordConfirm, successCallback, errorCallback) => {
    const data = {'old_pass':oldPassword,'password':password,'password_confirmation':passwordConfirm};
    fetch(`${baseurl}/users/password`, {
        method: 'POST',
        headers: {'Authorization':access_token, 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const userAddVehicles = (access_token,registration_number,vehicle_type_id,model,successCallback, errorCallback) => {
    const data = {'registration_number':registration_number,'vehicle_type_id':vehicle_type_id,'model':model};
    fetch(`${baseurl}/vehicles/add`, {
        method: 'POST',
        headers: { 
            'Authorization':access_token,
            'Content-Type': 'application/json', },
		body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}

const userRemoveVehicle = (vehicle_id,access_token,successCallback,errorCallback)=>{
    fetch(`${baseurl}/vehicles/status/${vehicle_id}`, {
        method: 'POST',
        headers: { 
            'Authorization':access_token,
            'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}


const userVehicles = (access_token ,successCallback, errorCallback) => {
    
    fetch(`${baseurl}/vehicles/list`, {
        method: 'GET',
        headers: { 'Authorization':access_token,'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const locationSearch = (search_query, successCallback, errorCallback) => {
    const data = {'search_query':search_query};
    fetch(`${baseurl}/locations/search/query`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const locationsAll = (successCallback, errorCallback) => {
    
    fetch(`${baseurl}/locations_all`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const locationByCity = (city_slug, successCallback, errorCallback) => {
    const data = {'city_slug':city_slug};
    fetch(`${baseurl}/locations_bycity`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}


const locationDetailsById = (locationID,successCallback, errorCallback) => {
    
    fetch(`${baseurl}/locations/getDetails/${locationID}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}


const locationVehicleType = (successCallback, errorCallback) => {
    
    fetch(`${baseurl}/locations/vehicletypes/${location_id}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const bookingRequest = (access_token,start_date,end_date,start_time,end_time,location_id,user_vehicle_id,vehicle_type_id, successCallback, errorCallback) => {
    const data = {
					'start_date':start_date,
					'end_date':end_date,
					'start_time':start_time,
					'end_time':end_time,
					'location_id':location_id,
					'user_vehicle_id':user_vehicle_id,
					'vehicle_type_id':vehicle_type_id
				};
    fetch(`${baseurl}/requests/create`, {
        method: 'POST',
        headers: { "Authorization":access_token,'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}


const pastBookings = (successCallback, errorCallback) => {
    
    fetch(`${baseurl}/requests/past`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}



const currentBooking = (access_token,successCallback, errorCallback) => {
    
    fetch(`${baseurl}/requests/list`, {
        method: 'GET',
        headers: {'Authorization':access_token, 'Content-Type': 'application/json', }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}


const allBookings = (access_token,successCallback, errorCallback) => {
    
    fetch(`${baseurl}/user/bookings`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
                'Authorization':access_token
    }
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}

const forgotPassword = (mobile_no, password,passwordConfirm, successCallback, errorCallback) => {
    const data = {'mobile_no':mobile_no,'password':password,'password_confirmation':passwordConfirm};
    fetch(`${baseurl}/forgot/password`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(data) });
}

const payment = (token,booking_id,payment_mode,successCallback,errorCallback)=>{
    const data = {'booking_id':booking_id,'payment_mode':payment_mode};
    fetch(`${baseurl}/payment/process`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 
                   'Authorization':token
    },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => { successCallback(data) })
        .catch((error) => { errorCallback(error) });
}

const getTransactionDetailByID = (token,txnid,successCallback,errorCallback)=>{
    console.log('token:'+token +' txnid:'+ txnid)
    fetch(`${baseurl}/requests/get/${txnid}`,{
        method:'GET',
        headers:{
            'Content-Type':'application/json',
            'Authorization':token
        }
    }).then(response=>response.json())
    .then(data=>{
        successCallback(data)
    }).catch(error=>{
        errorCallback(error)
    })
}

const socialLogin = (access_token,social_media,successCallback,errorCallback)=>{
    const data = {'social_token':access_token,'social_media':social_media}
    fetch(`${baseurl}/authenticate/social_media`,{
        method:'POST',
        headers:{
            'Content-Type':'application/json',
            
        },
        body:JSON.stringify(data)
    }).then(response=>response.json())
    .then(data=>{
        successCallback(data)
    }).catch(error=>{
        errorCallback(error)
    })
}
const userExists = (email,successCallback,errorCallback)=>{
    const data = {'email':email}
    fetch(`${baseurl}/user/exist`,{
        method:'POST',
        headers:{
            'Content-Type':'application/json',
            
        },
        body:JSON.stringify(data)
    }).then(response=>response.json())
    .then(data=>{
        successCallback(data)
    }).catch(error=>{
        errorCallback(error)
    })
}

const updateOnlinePayment = (access_token,booking_id,response,successCallback,errorCallback) =>{
    const data = {booking_id:booking_id,response:JSON.stringify(response)}
    console.log(JSON.stringify(data))
    fetch(`${baseurl}/payment/confirm`,{
        method:'POST',
        headers:{
            'Content-Type':'application/json',
            'Authorization':access_token
        },
        body:JSON.stringify(data)
    }).then(response=>response.json())
    .then(responseData=>{
        successCallback(responseData)
    }).catch(error=>{
        errorCallback(error)
    })

}

const cancelBooking = (access_token,request_id,successCallback,errorCallback)=>{
    const data = {request_id:request_id};
    console.log(request_id)
    fetch(`${baseurl}/requests/cancel`,{
        method:'POST',
        headers:{
            'Content-Type':'application/json',
            'Authorization':access_token
        },
        body:JSON.stringify(data)
    }).then(response=>response.json())
    .then(responseData=>{
        successCallback(responseData)
    }).catch(error=>{
        errorCallback(error)
    })
}

const googleLogin=(email,successCallback,errorCallback)=>{
    const data = {client_id:GOOGLE_CLIENT_ID,email:email};
   
    fetch(`${baseurl}/authenticate/google`,{
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        },
        body:JSON.stringify(data)
    }).then(response=>response.json())
    .then(responseData=>{
        successCallback(responseData)
    }).catch(error=>{
        errorCallback(error)
    })
}

export { 
		register,
		authenticate,
		getOtp,
		verifyOtp,
		userAccount,
		updatePassword,
		userAddVehicles,
		userVehicles,
		locationSearch,
		locationsAll,
		locationByCity,
		locationDetailsById ,
		locationVehicleType,
		bookingRequest,
		pastBookings,
		currentBooking,
        allBookings,
        forgotPassword,
        payment,
        getTransactionDetailByID,
        socialLogin,
        userExists,
        updateOnlinePayment,
        cancelBooking,
        googleLogin,
        baseurl,
        userRemoveVehicle
	}
