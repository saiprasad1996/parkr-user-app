const ACCESS_TOKEN = "access_token";
const LOGIN='LOGIN';
const LOGOUT='LOGOUT';
const GOOGLE='google';
const FACEBOOK = 'facebook';
const GOOGLE_CLIENT_ID = "883055851534-tqbj1qok3vphj9qd4es0up5m74bu2msj.apps.googleusercontent.com";

export {
    ACCESS_TOKEN,
    LOGIN,
    LOGOUT,
    GOOGLE,
    FACEBOOK,
    GOOGLE_CLIENT_ID,
}

export const ValidateRC = (rc)=>{
    if (/^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$/.test(rc)) {
       return (true)
    } 
     return (false)
   }
