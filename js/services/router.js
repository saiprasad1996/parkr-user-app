import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Splash from '../screens/Splash'
import Signup from '../screens/Signup'
import Login from '../screens/Login'
import Home from '../screens/Home'
import ForgotPassword from '../screens/ForgotPassword'
import Bookings from '../screens/Bookings'
import Subscriptions from '../screens/Subscriptions'
import Vehicles from '../screens/Vehicles'
import Profile from '../screens/Profile'
import PasswordUpdate from '../screens/PasswordUpdate';
import React from 'react'
import LogoTitle from '../components/LogoTitle'
import Book from '../screens/Book'
import Payment from '../screens/Payment'
import Ticket from '../screens/Ticket'
import NewPassword from '../screens/NewPassword'
import SideBar from '../screens/SideBar'
// import AllComponents from '../screens/AllComponents'
import AddVehicle from '../screens/AddVehicle';
import TitleHeader from '../components/TitleHeader';
const screenWidth = Dimensions.get("window").width;
import {Dimensions,TouchableOpacity,StyleSheet,Image,View,} from 'react-native'
import Logo from '../assets/images/logo.png';
import PaymentSuccess from '../screens/PaymentSuccess';
import PaymentError from '../screens/PaymentError'
import Otp from '../screens/Otp';

import Icon from 'react-native-vector-icons/FontAwesome';
// import SidebarContainer from '../screens/SidebarContainer';

const DrawerRouteConfig = {
    Home:{
        screen:Home,
    },

    PaymentSuccess:{
        screen:PaymentSuccess
    },
    PaymentError:{
        screen:PaymentError
    }
}

const DrawerConfig = {
    drawerType:"slide",
    contentComponent:SideBar
}

const Drawer=createDrawerNavigator(
    DrawerRouteConfig,
    DrawerConfig, 
)

const AppNavigator = createStackNavigator({
    
    Splash: {
      screen : Splash,
      navigationOptions:{
        headerShown:false
    }
    },
    Signup:{
        screen : Signup,
        navigationOptions:{
            headerShown:false
        }
    },
    ForgotPassword:{
        screen : ForgotPassword,
        navigationOptions:{
            headerShown:false
        }
    },
    Bookings:{
        screen:Bookings,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"My Bookings"} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
   
    Payment:{
        screen:Payment,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"Payment"} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    Book:{
        screen:Book,
        navigationOptions:({navigation})=>({
            headerTitle:()=><LogoTitle  />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    Login:{
        screen : Login,
        navigationOptions:{
            headerShown:false
        }
    },
    Profile:{
        screen : Profile,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"Profile"} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    PasswordUpdate:{
        screen : PasswordUpdate,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"Update"} />,
            headerLeft: <View style={{marginLeft:80}} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    AddVehicle:{
        screen : AddVehicle,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"Add Vehicle"} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    Ticket:{
        screen : Ticket,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"Ticket"} />,
            headerLeft: <View style={{marginLeft:70}} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    Vehicles:{
        screen : Vehicles,
        navigationOptions:({navigation})=>({
            headerTitle:()=><TitleHeader title={"Vehicles"} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71}
        })
    },
    Subscriptions:{
        screen:Subscriptions,
        navigationOptions:{
            headerShown:false,
            header:null,
        }
    },
    Otp:{
        screen:Otp,
        navigationOptions:{
            headerShown:false,
            header:null,
        }
    },
    NewPassword:{
        screen:NewPassword,
        navigationOptions:{
            headerShown:false,
            header:null,
        }
    },
    Home:{
        screen:Home,
        navigationOptions:({ navigation }) => ({
            headerTitle: () => <View style={{ marginLeft:50,flex: 1, flexDirection: "row" }}>
            <Image
              source={require('../assets/images/logo.png')}
              style={{ resizeMode: 'contain', width: 65, height: 35 }}
            />
            <TouchableOpacity><Icon style={{fontSize:20,marginTop:5,marginLeft:Dimensions.get('window').width-200}} name="bell" /></TouchableOpacity>
            <TouchableOpacity onPress={()=>{
              this.props.navigation.navigate('Subscriptions')
            }}><Icon style={{fontSize:20,marginTop:5,marginLeft:35}} name="ellipsis-v" /></TouchableOpacity>
          </View>,
            headerStyle: { borderBottomEndRadius:30,borderBottomLeftRadius:30,height:71 },
            
          })
    },
    HomeDrawer:{
        screen:Drawer,
        navigationOptions:({navigation})=>({
            headerTitle:()=><LogoTitle withDrawer={true} title={"Ticket"} />,
            headerStyle: {borderBottomEndRadius:30, borderBottomLeftRadius:30,height:71},
            headerLeft:null
        }),
        
    }
    
  },
  );
  
  export default createAppContainer(AppNavigator);


const styles = StyleSheet.create({
    logo: {
      width: 100,
      resizeMode: 'contain',
    },
});