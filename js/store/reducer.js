import {LOGIN, LOGOUT} from './constants';
const initialState = {
  logged_in: false,
  access_token:null,
  phone:null
};
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        logged_in: true,
        access_token:action.payload.access_token,
        phone:action.payload.phone
      };
    case LOGOUT:
      return {
        ...state,
        logged_in: false,
        access_token:null,
        phone:null
      };
    default:
      return state;
  }
};
export default authReducer;
